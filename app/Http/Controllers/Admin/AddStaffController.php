<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AddStaff;
use Illuminate\Support\Facades\Validator;


class AddStaffController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->model = new AddStaff();
    }

    public function index()
    {
        $addstaff = AddStaff::all();
        return view(parent::loadCommonDataToView('admin.addstaff.index'),[
            'addstaff' => $addstaff
        ]);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view(parent::loadCommonDataToView('admin.addstaff.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|max:40',
            'address' => 'required|max:40',
            'type_work' =>'max:250',
            
            'description' => 'max:250',
        ]);


       $addstaff = new addstaff();

        
        $addstaff->name = $request->input('name');
        $addstaff->address= $request->input('address');
        $addstaff->type_work= $request->input('type_work');
        $addstaff->description = $request->input('description');
        $addstaff->save();

        return redirect('admin/addstaff')->with('success_message' , 'New staff has been added Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $addstaff = AddStaff::find($id);
        return view(parent::loadCommonDataToView('admin.addstaff.edit'),[
            'addstaff' => $addstaff
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $addstaff = AddStaff::find($id);

        
        $addstaff->name = $request->input('name');
        $addstaff->address= $request->input('address');
        $addstaff->type_work= $request->input('type_work');
        $addstaff->description = $request->input('description');
        $addstaff->save();


        return  redirect()->back()->with('success_message', 'New staff has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
         $addstaff = AddStaff::find($id);
         $addstaff->destroy();
        return redirect()->back()->with('success_message', 'Staff has been successfully destroy.');

         
    }
}
