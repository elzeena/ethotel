<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Amenity;
use App\Http\Requests\Amenity\AddAmentiyRequest;

class AmenityController extends BaseController
{
    protected $model;
    public function __construct(Amenity $amenity){
        $this->model  = $amenity;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = [];
        $data['amenities'] = $this->model->all(); 
        return view(parent::loadCommonDataToView('admin.amenity.index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView('admin.amenity.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'title'=>'required | max:40',
            'content'=>'max:200',
            'rank'=>'required | numeric'
        ]);

        $reqData = $request->all();
        $this->model->create([
            'title' => $request->get('title'),
            'numbers' => $request->get('numbers'),
            'content' => $request->get('content'),
            'rank' => $request->get('rank')
        ]);
       return  redirect()->back()->with('success_message', 'New Amenity has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($request, $id)
    {
        $this->validate($request,[
            'title'=>'required | max:40',
            'content'=>'max:200',
            'rank'=>'required | numeric'
        ]);
        
        $amenity = Amenity::find($id);
        return view('admin.amenity.edit',[
            'amenity' => $amenity
        ]);

        $data['row']  = Amenity::find($id);
        return view(parent::loadCommonDataToView('admin.amenity.edit',compact('data',$data['row'])));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
