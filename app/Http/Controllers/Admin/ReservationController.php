<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Reservation;
use Illuminate\Support\Facades\Validator;
use App\Models\Room;


class ReservationController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::all();
            return view(parent::loadCommonDataToView('admin.reservation.index'), [
            'reservations' => $reservations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::select('rt.title','rooms.id')
                    ->join('room_types as rt', 'rt.id','=','rooms.room_type_id')
                    ->get()->pluck('title','id')->toArray();
        
        return view(parent::loadCommonDataToView('admin.reservation.create'),compact('rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());


        $this->validate($request,[
            'name' => 'required|max:40',
            'email' => 'email|max:200',
            'address' => 'required|max:40',
            'phone' => 'required',
            'arrival_date' => 'required|date|date_format:Y-m-d',
            'departure_date' => 'required|date|date_format:Y-m-d|after:arrival_date',
            'num_adults' => 'required|numeric',
            'num_kids' => 'required|numeric',
            'gender' => 'required',
            'total_amt' => 'required'

        ]);

        $reservations = new Reservation();

        //   if ($request->hasFile('image')) {
        //     $image = $request->file('image');
        //     $ext = $image->getClientOriginalExtension();
        //     $destination = 'images/reservations';
        //     $photo_name = md5(time());
        //     $photo_original_name = $destination . '/' . $photo_name . '.' . $ext;
        //     $image->move($destination, $photo_original_name);
        //     $reservations->image = $photo_original_name;
        // }

        $reservations->name = $request->input('name');
        $reservations->email = $request->input('email');
        $reservations->address = $request->input('address');
        $reservations->phone = $request->input('phone');
        $reservations->arrival_date = $request->input('arrival_date');
        $reservations->departure_date = $request->input('departure_date');
        $reservations->num_adults = $request->input('num_adults');
        $reservations->num_kids = $request->input('num_kids');
        $reservations->gender = $request->input('gender');
        $reservations->room_id = 0;
        $reservations->total_amt = $request->input('total_amt');
        $reservations->save();

        return  redirect()->back()->with('success_message', 'New Bookings has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

         $rooms = Room::select('rt.title','rooms.id')->join('room_types as rt', 'rt.id','=','rooms.room_type_id')->get()->pluck('title','id')->toArray();
        
        $reservations = Reservation::find($id);
       return view(parent::loadCommonDataToView('admin.reservation.edit'),[
            'reservations' => $reservations,
            'rooms' => $rooms
   ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $reservations = Reservation::find($id);

        // if ($request->hasFile('image')) {
        //     $image = $request->file('image');
        //     $destination = 'images/room';
        //     $extension = $image->getClientOriginalExtension();
        //     $image_name = $destination . '/' . md5(time()) . '.' . $extension;

        //     if ($reservations->image && app('files')->exists($rooms->image)) {
        //         app('files')->delete($reservations->image);
        //     }

        //     $image->move($destination, $image_name);
        //     $reservations->image = $image_name;
        // }

        $reservations->name = $request->input('name');
        $reservations->email = $request->input('email');
        $reservations->address = $request->input('address');
        $reservations->phone = $request->input('phone');
        $reservations->arrival_date = $request->input('arrival_date');
        $reservations->departure_date = $request->input('departure_date');
        $reservations->num_adults = $request->input('num_adults');
        $reservations->num_kids = $request->input('num_kids');
        $reservations->gender = $request->input('gender');
        $reservations->room_id = 0;
        $reservations->total_amt = $request->input('total_amt');
        $reservations->save();

        return  redirect()->back()->with('success_message', 'New Reservation has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = Reservation::find($id);
        $row->delete();
        return redirect()->route('admin.reservation.index')->with('error_message','Reservation Deleted successfully!!!!');
    }


    public function publish($id)
    {
        $reservations = Reservation::find($id);
        $reservations->status = $reservations->status ? 0 : 1;
        $reservations->save();
        return response()->json(['response'=>$reservations],200);
    } 

    public function genderone($id)
    {
        $reservations = Reservation::find($id);
        $reservations->gender = $reservations->gender ? 0 : 1;
        $reservations->save();
        return redirect()->back();
    }
}
