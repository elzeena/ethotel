<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Room;
use App\Http\Requests\Admin\Room\AddRoomRequest;
use App\Http\Requests\Admin\Room\EditRoomRequest;


class RoomController extends BaseController
{
    protected $room_types;


    public function __construct()
    {
        $this->room_types = ['' => 'Select Option'] + RoomType::pluck('title', 'id')->toArray();
        $this->model = new Room();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rooms = Room::all();
        // dd($rooms);
            return view(parent::loadCommonDataToView('admin.room.index'), [
            'rooms' => $rooms,
        ]);
         }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView('admin.room.create'),[
            'room_types' => $this->room_types,

        ]);
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'room_code'=>'required|unique:rooms',
            'excerpt'=>'required|max:250',
            'description'=>'max:600',
            'sell_price'=>'numeric',
            'regular_price'=>'required|numeric',
            'room_type_id'=>'required|exists:room_types,id',
            'image'=>'required|mimes:jpeg,bmp,png',
            'no_bed'=>'required|numeric',

        ]);

        $room = new Room();


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $destination = 'images/room';
            $photo_name = md5(time());
            $photo_original_name = $destination . '/' . $photo_name . '.' . $ext;
            $image->move($destination, $photo_original_name);
            $room->image = $photo_original_name;
        }
        $room->room_code = $request->input('room_code');
        $room->description = $request->input('description');
        $room->sell_price = $request->input('sell_price');
        $room->regular_price = $request->input('regular_price');
        $room->room_type_id = $request->input('room_type_id');
        $room->no_bed = $request->input('no_bed');
        $room->excerpt = $request->input('excerpt');
        $room->save();

        return  redirect()->back()->with('success_message', 'New Room has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $rooms = Room::find($id);
       return view(parent::loadCommonDataToView('admin.room.edit'),[
            'room_types' => $this->room_types,
             'rooms' => $rooms
   ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'room_code'=>'required',
            'excerpt'=>'required|max:250',
            'description'=>'max:600',
            'sell_price'=>'required_without:regular_price|numeric',
            'regular_price'=>'required_without:sell_price|numeric',
            'room_type_id'=>'required',
            //'category_id' => 'required|exists:categories,id'
            'image' => 'nullable|image|max:3000',
            'no_bed'=>'required|numeric',

        ]);
        //dd($request->all());
        $rooms = Room::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $destination = 'images/room';
            $extension = $image->getClientOriginalExtension();
            $image_name = $destination . '/' . md5(time()) . '.' . $extension;

            if ($rooms->image && app('files')->exists($rooms->image)) {
                app('files')->delete($rooms->image);
            }

            $image->move($destination, $image_name);
            $rooms->image = $image_name;
        }
        $rooms->room_code = $request->input('room_code');
        $rooms->description = $request->input('description');
        $rooms->sell_price = $request->input('sell_price');
        $rooms->regular_price = $request->input('regular_price');
        $rooms->room_type_id = $request->input('room_type_id');
        $rooms->no_bed = $request->input('no_bed');
        $rooms->excerpt = $request->input('excerpt');
        $rooms->includes = $request->input('includes');
        $rooms->status = $request->input('status');
        $rooms->save();

        return  redirect()->back()->with('success_message', 'New Room has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        $row->delete();

        return redirect()->route('admin.room.index')->with('error_message','Room Deleted successfully!!');
    }
}
