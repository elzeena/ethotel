<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\RoomType;
use Illuminate\Support\Facades\Validator;


class RoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->model = new RoomType();
    }

    public function index()
    {
        $room_types = [];
        $room_types['rows'] = $this->model->all();

        return view(parent::loadCommonDataToView('admin.roomtypes.index',compact('room_types')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view(parent::loadCommonDataToView('admin.roomtypes.create',compact('room_types')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title' => 'required|max:40',
            'description' => 'required|max:250'
        ]);


       $this->model->create([
        'title' => $request->get('title'),
        'slug' => str_slug($request->get('title')),
        'description' =>$request->get('description'),
       ]);

       return redirect()->route('admin.roomtypes.index')->with('success_message','Room type added successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $roomtypes = RoomType::find($id);
       return view(parent::loadCommonDataToView('admin.roomtypes.edit'),[
            'room_types' => $roomtypes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
       $roomtypes = RoomType::find($id);
       $roomtypes = $this->model->find($id);

        $roomtypes->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);

        $roomtypes->save();

        return redirect()->route('admin.roomtypes.index')->with('success_message','Room type Updated successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
         $row = $this->model->find($id);
        $row->delete();
        return redirect()->route('admin.roomtypes.index')->with('error_message','Room type Deleted successfully!!!!');
    }
}
