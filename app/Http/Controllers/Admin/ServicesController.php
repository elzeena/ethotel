<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Requests\Services\AddServiceRequest;


class ServicesController extends BaseController
{
    protected $model;
    public function __construct(Service $service){
        $this->model  = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['services'] = $this->model->all(); 
        return view(parent::loadCommonDataToView('admin.service.index',compact('data')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView('admin.service.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = $this->model;
        $requestData = $request->all();
        // dd($requestData);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $destination = 'images/services';
            $photo_name = md5(time());
            $photo_original_name = $destination . '/' . $photo_name . '.' . $ext;
            $image->move($destination, $photo_original_name);
            $requestData['image'] = $photo_original_name;
        }
        $service->create([
                'title' => $requestData['title'],
                'short_description' => $requestData['short_description'],
                'content' => $requestData['content'],
                'service_img'=> isset($requestData['image'])? $requestData['image']:'',
                'rank'=> $requestData['rank'],
        ]);

        return  view(parent::loadCommonDataToView('admin.service.index'))->with('success_message', 'New Service has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data['row'] = $this->model::find($id);
        return view(parent::loadCommonDataToView('admin.service.edit',compact('data')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = $this->model->find($id);

        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ext = $image->getClientOriginalExtension();
            $destination = 'images/services';
            $photo_name = md5(time());
            $photo_original_name = $destination . '/' . $photo_name . '.' . $ext;
            $image->move($destination, $photo_original_name);
            $requestData['image'] = $photo_original_name;
            if($service->service_img){
                $old_image = $service->service_img;
                $path = $old_image;
                if (file_exists($path))
                    unlink($path);
            }
        }else{
             $requestData['image'] = $service->service_img;
        }

        $service->update([
                'title' => $requestData['title'],
                'short_description' => $requestData['short_description'],
                'content' => $requestData['content'],
                'service_img'=> $requestData['image'],
                'rank'=> $requestData['rank']
        ]);

        return redirect()->route('admin.service.index')->with('success_message',' Service Updated successfully!!!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
