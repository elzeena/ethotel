<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Slider;

class SliderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $slider = Slider::all();
        return view(parent::loadCommonDataToView('admin.slider.index'), [
            'slider' => $slider,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView('admin.slider.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:40',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        $slider = new slider();

        if ($request->hasfile('image')) {
            $image =$request->file('image');
            $ext=$image->getClientOriginalExtension();
            $destination = 'images/slider';
            $photo_name = md5(time());
            $photo_original_name = $destination . '/' . $photo_name . '.' . $ext;
            $image->move($destination, $photo_original_name);
            $slider->image = $photo_original_name;
        }
        $slider->title = $request->input('title');
        
        $slider->links = $request->input('links');
        $slider->caption = $request->input('caption');
        $slider->status = $request->input('status');
        $slider->rank = $request->input('rank');
        $slider->save();

        return redirect('admin/slider')->with('success_message' , 'New Slider has been added Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $slider = Slider::find($id);
        return view(parent::loadCommonDataToView('admin.slider.edit'),[
            'slider' => $slider
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $destination = 'images/sliders';
            $extension = $image->getClientOriginalExtension();
            $image_name = $destination . '/' . md5(time()) . '.' . $extension;

            if ($slider->image && app('files')->exists($slider->image)) {
                app('files')->delete($slider->image);
            }

            $image->move($destination, $image_name);
            $slider->image = $image_name;
        }
        $slider->title = $request->input('title');
        $slider->links= $request->input('links');
        $slider->caption = $request->input('caption');
        $slider->rank = $request->input('rank');
                $slider->status = $request->input('status');

        $slider->save();

        return  redirect()->back()->with('success_message', 'New slider has been Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function publish($id)
    {
        $slider = Slider::find($id);
        $slider->status = $slider->status ? 0 : 1;
        $slider->save();
        return redirect()->back();;
    } 
    public function destroy(Request $request, $id)
    {
        $slider = Slider::find($id);

        if ($slider->image && app('files')->exists($slider->image)) {
            app('files')->delete($slider->image);
        }
        $slider->delete();
        return redirect()->back()->with('success_message', 'Slider has been successfully deleted.');
    }
}
