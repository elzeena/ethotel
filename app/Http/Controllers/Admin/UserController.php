<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;



class UserController extends BaseController
{

    public function profile()
    {
        $data = [];
        $data['user'] = auth()->user();
        return view(parent::loadCommonDataToView('admin.user.profile'), compact('data'));
    }


    public function profileUpdate(Request $request)
    {
        //dd($request->all());
        $this->validateLogin($request);
        // dd($request->all());
        $user = auth()->user();

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->middle_name = $request->get('middle_name');
        $user->last_name = $request->get('last_name');
        $user->contact = $request->get('contact');
        $user->address = $request->get('address');
        if($request->get('password')){
            $user->password = bcrypt($request->get('password'));
        }
       
        $user->save();


        return redirect()->route('admin.profile')->with('success_message','You successfully Updated your profile.');
    }

    protected function validateLogin(Request $request)
    {

        $rules =  [
            'name' => 'required|string|unique:users,name,'.auth()->user()->id,
            'email' => 'required|string|unique:users,email,'.auth()->user()->id,
        ];

        if($request->get('password')){

            $rules['password'] = 'confirmed|string|min:6';

        }

        $this->validate($request,$rules);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

    }
}
