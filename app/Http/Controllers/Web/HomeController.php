<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Service;
use App\Models\Room;
use App\Models\Inquiry;
use App\Models\About;
use Illuminate\Support\Facades\Validator;



class HomeController extends Controller
{
	public function index(){
		$data['slider'] = Slider::HomeSlider();
		$data['services'] = Service::HomeServices();
		$data['abouts'] = About::HomeAboutUs();
		$data['rooms'] = Room::HomeRooms();
		return view('frontend.home.index',compact('data'));
	}

	public function aboutUs(){
		$data['abouts'] = About::all();
		return view('frontend.about-us.index',compact('data'));
	}
	
	public function services(){
		$data['services'] = Service::all();
		return view('frontend.services.index',compact('data'));
	}

	public function rooms(){
		$data['rooms'] = Room::all();
		return view('frontend.rooms.index',compact('data'));
	}

	public function contact(){
		return view('frontend.contact.index',compact('data'));
	}

	public function submitContactForm(Request $request){
		
		$inquiry = new Inquiry();
		$inquiry->create($request->all());
		return redirect()->back()->with(['message'=>'Your Message Has Been Send. Our Staff will responds you back.']);
	}
}
