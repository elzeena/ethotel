<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Service;
use App\Models\Room;
use App\Models\RoomType;
use App\Models\Inquiry;
use Illuminate\Support\Facades\Validator;


class RoomsController extends Controller
{
	public function index(){
        $data['rooms'] = Room::all();
        return view('frontend.rooms.index',compact('data'));
	}

    public function getSingleRoom($id)
    {
        $rooms = Room::find($id);
        return view('frontend.rooms.roomdetails',[
            'room' => $rooms
          ]);
    }
    public function booking(Request $request, $id){
        $rooms = Room::find($id);
        $arrival_date = $request->input('arrival_date');
        $departure_date = $request->input('departure_date');
        $num_adults = $request->input('num_adults');
        $num_kids = $request->input('num_kids');
        $room_type = $request->input('room_type');
        return view('frontend.rooms.reservation',[
            'room' => $rooms,
            'arrival_date' => $arrival_date,
            'departure_date' => $departure_date,
            'num_adults' => $num_adults,
            'num_kids' => $num_kids,
            'room_type' => $room_type


        ]);
    }


    public function publish($id)
    {
        $reservations = Reservation::find($id);
        $reservations->status = $reservations->status ? 0 : 1;
        $reservations->save();
        return redirect()->back();
    }


}
