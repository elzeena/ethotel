<?php

namespace App\Http\Requests\Admin\Room;

use Illuminate\Foundation\Http\FormRequest;

class AddRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // regex:/^(\d*([.,](?=\d{3}))?\d+)+((?!\2)[.,]\d\d)?$/
        return [
        'room_code'=>'required|unique:rooms',
        'excerpt'=>'required|max:250',
        'description'=>'max:600',
        'sell_price'=>'numeric',
        'regular_price'=>'required|numeric',
        'room_type_id'=>'required|exists:room_types,id',
        'image'=>'required|mimes:jpeg,bmp,png',
        'no_bed'=>'required|numeric',
        ];
    }
}
