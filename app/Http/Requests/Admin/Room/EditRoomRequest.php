<?php

namespace App\Http\Requests\Admin\Room;

use Illuminate\Foundation\Http\FormRequest;

class EditRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // regex:/^(\d*([.,](?=\d{3}))?\d+)+((?!\2)[.,]\d\d)?$/
        return [
        'room_code'=>'required',
        'excerpt'=>'required|max:250',
        'description'=>'max:600',
        'sell_price'=>'required_without:regular_price|numeric',
        'regular_price'=>'required_without:sell_price|numeric',
        'room_type_id'=>'required',
        //'category_id' => 'required|exists:categories,id'
        'image' => 'nullable|image|max:3000',
        'no_bed'=>'required|numeric',
        ];
    }
}
