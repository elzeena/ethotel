<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddStaff extends Model
{
    protected $table = 'add_staff';
    protected $fillable = ['name','address','type_work','description']; 
    protected $guarded = ['id','created_at','updated_at'];
}
