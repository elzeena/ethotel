<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
	protected $table =  'amentities';
    protected $fillable = ['title','numbers','content','rank'];

    public function rooms(){
    	return $this->belongsTo('App\Models\Room','id','room_id');
    }
}
