<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Html;

class Profile extends Model
{
    protected $table = 'users';
    protected $fillable = ['name','email','password','image'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function pathToImage($path) {
        if(filter_var($path,FILTER_VALIDATE_URL)){
            $path_ = $path;
        }
        else{
            $path_ = $path?url('/').'/'.$path:"";
        }
        return $path_;
    }
    public function renderImage(array $option =null) {
        return Html::image($this->pathToImage($this->image),$this->name,$option);
    }
}
