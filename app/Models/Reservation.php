<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Room;


class Reservation extends Model
{
    protected $fillable = ['name','email','phone','address','image','gender','arrival_date','departure_date','num_adults','num_kids','room_id','total_amt','status'];

    public function room(){
   	return $this->belongsTo('App\Models\Room');
    }


    public function publish(){
        if($this->status ==1){
            return '<a href="'.route('admin.reservation.publish',$this->id).'"> <span class="label label-sm label-success">Active</span></a>';
        }else{
            return '<a href="'.route('admin.reservation.publish',$this->id).'"> <span class="label label-sm label-danger">Inactive</span></a>';
        }
    }

     public function genderone(){
        if($this->gender ==1){
            return '<a href="'.route('admin.reservation.genderone',$this->id).'"> <span class="label label-sm label-success">male</span></a>';
        }else{
            return '<a href="'.route('admin.reservation.genderone',$this->id).'"> <span class="label label-sm label-danger">female</span></a>';
        }
    }
}
