<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Html;

class Room extends Model
{
    protected $table = 'rooms';

    protected $fillable = ['room_code','description','sell_price','regular_price','room_type_id','featured_photo','no_bed','excerpt','amentity_id'];


    public function room_types()
    {
        return $this->belongsTo('App\Models\RoomType','room_type_id','id');
    }
    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation','room_id','id');
    }
    public function amenities()
    {
        return $this->hasMany('App\Models\Amenity','id','amentity_id');
    }
    public function pathToImage($path) {
        if(filter_var($path,FILTER_VALIDATE_URL)){
            $path_ = $path;
        }
        else{
            $path_ = $path?url('/').'/'.$path:"";
        }
        return $path_;
    }
    public function renderImage(array $option =null) {
        return Html::image($this->pathToImage($this->image),$this->name,$option);
    }

    public function scopeHomeRooms($query){
        return $query->orderBy('created_at')->take(6)->get();
    }

    public function getExcerptAttribute($value){
        return substr($value,0,100).' ....';
    }
    


}
