<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $table = 'room_types';
    protected $fillable = ['title','slug','description']; 
    protected $guarded = ['id','created_at','updated_at'];

    public function room(){
   	return $this->hasMany('App\Models\Room','room_id','id');
    }
    public function getTitleAttribute($value){
        return ucwords($value);
    }

}
