<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
   protected $fillable = ['title','short_description','content','service_img','rank'];

   public function scopeHomeServices($query){
   	return $query->orderBy('rank')->take(6)->get();
	}
}
