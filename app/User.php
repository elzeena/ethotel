<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Html;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','first_name','middle_name','last_name','contact','address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pathToImage($path) {
        if(filter_var($path,FILTER_VALIDATE_URL)){
            $path_ = $path;
        }
        else{
            $path_ = $path?url('/').'/'.$path:"";
        }
        return $path_;
    }
    public function renderImage(array $option=null) {
        return Html::image($this->pathToImage($this->image),$this->name,$option);
    }

}
