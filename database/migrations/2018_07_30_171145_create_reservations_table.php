<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        
         Schema::dropIfExists('reservations');
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('email',20)->unique();
            $table->string('gender');
            $table->string('phone');
            $table->string('address');
            $table->string('image')->default(0);
            $table->integer('room_id');
            $table->integer('room_type_id');
            //$table->foreign('room_id')->references('id')->on('room_types')->onDelete('cascade');
            $table->date('arrival_date');
            $table->date('departure_date');
            $table->tinyInteger('num_adults')->unsigned();
            $table->tinyInteger('num_kids')->unsigned();
            $table->float('total_amt')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
