<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
        	'name' => 'Super Admin',
        	'email' => 'root@hotel.local',
            'first_name' => 'elzee',
            'last_name' => 'dangol',
            'contact' => '984111111',
            'address' => 'koteswhor',
            'password_confirm' =>bcrypt('h@ppy'),
        	'password' => bcrypt('h@ppy'),
        ]);
    }
}
