
@extends('admin.includes.layout')

@section ('content')
      <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Edit staff details</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                @include('admin.includes.breadcrumb_dashboard_link')
                                <li><a class="parent-item" href="#">Staff</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Edit staff Details</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
            <div class="col-sm-12">
             <div class="card-box">
                  <div class="card-head">
                    <header>Add Staff</header>
                    <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                  </div>
                  <div class="card-body row" id="bar-parent">
                          {!! Form::open(array(
                    'route' => ['admin.addstaff.update',$addstaff->id],
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'files' => true,
                    'method' => 'PUT',
                    )) !!}
                    @include('admin.addstaff.includes.form')
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Edit Staff</button>
                            <button type="reset" class="btn btn-danger"><i class="fa fa-recycle"></i> Cancel</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            @include('admin.includes.flash_messages')
        </div>
    </div>
</div>
@endsection