<div class="form-group">
    {!! Form::label('name','Name',['class'=>'form-label']) !!}
    {!! Form::text('name',isset($addstaff->name)?$addstaff->name:'' , ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('address','Address',['class'=>'form-label']) !!}
    {!! Form::text('address',isset($addstaff->address)?$addstaff->address:'' , ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('type_work','Type of work',['class'=>'form-label']) !!}
    {!! Form::text('type_work',isset($addstaff->type_work)?$addstaff->type_work:'' , ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('description','Description', ['class'=>'form-label']) !!}
    {!! Form::text('description',isset($addstaff->description)?$addstaff->description:'', ['class'=>'form-control']) !!}
</div>

