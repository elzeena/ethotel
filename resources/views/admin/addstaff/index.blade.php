
@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">

            @include('admin.includes.flash_messages')

            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Staff</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Staff</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-12">
                <div class="card card-topline-green">
                                        <div class="card-head">
                                            <header>Staffs Available</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
			                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
			                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="table-scrollable">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>SN</th>
                                                            <th>Name</th>
                                                         
                                                            <th>Address</th>
                                                            <th>Type of Work</th>
                                                            <th>Description</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    	@foreach($addstaff as $addstaff)
                                                        <tr>
                                                            <td>{{ $addstaff->id }}</td>
                                                            <td>{{ $addstaff->name  }}</td>
                                                            
                                                            <td>{{ $addstaff->address  }}</td>
                                                            <td>{{ $addstaff->type_work }}</td>
                                                            <td>{{ $addstaff->description }}</td>
                                                    <td> <a href="{{ route('admin.addstaff.edit', $addstaff->id) }}"
                                                           class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                
                                                        <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                    </a>

                                                        

                                                    
                                                    </td>
                                                        
                                                        </tr>
                                                        @endforeach
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
           	</div>
           </div>
            
        </div>
    </div>

@endsection

@section('extra_scripts')

<script>

     $(".bootbox-confirm").on('click', function () {
            var $this = $(this);
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    $this.closest('div').find('form').submit();
                }
            });
            return false;
        });

</script>

@endsection


