<div class="form-group">
    <label for="title">Title </label>
    <input type="text" class="form-control" value="{{(isset($data['row']) && $data['row']->title)? $data['row']->title:old('title') }}" name="title" placeholder="Enter Title" >
</div>
<div class="form-group">
    <label for="numbers">Numbers</label>
    <input type="numeric" class="form-control" value="{{(isset($data['row']) && $data['row']->numbers)? $data['row']->numbers:old('numbers') }}" name="numbers" placeholder="Enter Title" >
</div>
<div class="form-group">
    <label for="rank">Position</label>
    <input type="numeric" name="rank" value="{{(isset($data['row']) && $data['row']->rank)? $data['row']->rank:old('rank') }}" class="form-control" placeholder="Enter Postion" >
</div>
<div class="form-group">
    <label for="content">Content</label>
    <textarea name="content" class="form-control" placeholder="Enter Contents" >{{(isset($data['row']) &&$data['row']->content)? $data['row']->content:old('content') }}</textarea>
</div>
