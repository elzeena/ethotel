@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success_message'))
        <div class="alert alert-success">
              <strong>Well done!</strong>
               {{ session()->get('success_message') }}
        </div>
       
    
@endif
