<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<!-- Mirrored from radixtouch.in/templates/admin/hotel/source/layout_full_width.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Jun 2018 08:47:32 GMT -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>:: Hotel Management ::</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="{{ asset('assets/admin/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <!--bootstrap -->
    <link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <!-- morris chart -->
    <link href="{{ asset('assets/admin/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/material/material.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/material_style.css')}}">
    <!-- animation -->
    <link href="{{ asset('assets/admin/css/pages/animate_page.css') }}" rel="stylesheet">
    <!-- Template Styles -->
    <link href="{{ asset('assets/admin/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
             <link rel="stylesheet" href="{{ asset('assets/admin/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css')}}" />
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="{{ asset('assets/admin/resources/demos/style.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/admin/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css')}}" />

    <!-- favicon -->
    {{-- <link rel="shortcut icon" href="assets/img/favicon.ico" /> --}}
    @yield('extra_styles')
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    @include('admin.includes.header')
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        @include('admin.includes.sidebar')
        <!-- end sidebar menu --> 
        @yield ('content')
    </div>
    <!-- end sidebar menu -->
    <!-- start page content -->

    <!-- end page content -->
    {{-- @include('admin.includes.sidebar') --}}
<!-- end page container -->
<!-- start footer -->
    @include('admin.includes.footer')
<!-- end footer -->
<!-- start js include path -->
<script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}" ></script>
<script src="{{asset('assets/admin/plugins/popper/popper.min.js')}}" ></script>
<script src="{{asset('assets/admin/plugins/jquery-blockui/jquery.blockui.min.js')}}" ></script>
<script src="{{asset('assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
<script src="{{asset('assets/admin/plugins/sparkline/jquery.sparkline.min.js')}}" ></script>
<script src="{{asset('assets/admin/js/pages/sparkline/sparkline-data.js')}}" ></script>
<!-- Common js-->
<script src="{{asset('assets/admin/js/app.js')}}" ></script>
<script src="{{asset('assets/admin/js/layout.js')}}" ></script>
<script src="{{asset('assets/admin/js/theme-color.js')}}" ></script>
<!-- material -->
<script src="{{asset('assets/admin/plugins/material/material.min.js')}}"></script>
<!-- animation -->
<script src="{{asset('assets/admin/js/pages/ui/animations.js')}}" ></script>
<script  src="{{asset('assets/admin/plugins/material-datetimepicker/moment-with-locales.min.js')}}"></script>
<script  src="{{asset('assets/admin/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js')}}"></script>
<script  src="{{asset('assets/admin/plugins/material-datetimepicker/datetimepicker.js')}}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- morris chart -->
{{-- <script src="{{asset('assets/admin/plugins/morris/morris.min.js')}}" ></script>
<script src="{{asset('assets/admin/plugins/morris/raphael-min.js')}}" ></script>
<script src="{{asset('assets/admin/js/pages/chart/morris/morris_home_data.js')}}" ></script>
 --}}<!-- end js include path -->
</div>
@yield('extra_scripts')

</body>

<!-- Mirrored from radixtouch.in/templates/admin/hotel/source/layout_full_width.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 25 Jun 2018 08:49:46 GMT -->
</html>