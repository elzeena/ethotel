<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- start User Profile menu -->
            <li class="sidebar-user-panel">
                <div class="user-panel">
                    <div class="row">
                        <div class="sidebar-userpic">
                            <img src="{{ asset('assets/admin/img/dp.jpg') }}" class="img-responsive" alt="">
                        </div>
                    </div>
                    <div class="profile-usertitle">
                        <div class="sidebar-userpic-name"> {{Auth::user()->title}} </div>
                        <div class="profile-usertitle-job"> Manager </div>
                    </div>
                    <div class="sidebar-userpic-btn">
                            <a class="tooltips" href="{{ route('admin.profile') }}" data-placement="top" data-original-title="Profile">
                                <i class="material-icons">person_outline</i>
                            </a>
                            <a class="tooltips" href="email_inbox.html" data-placement="top" data-original-title="Mail">
                                <i class="material-icons">mail_outline</i>
                            </a>
                            <a class="tooltips" href="chat.html" data-placement="top" data-original-title="Chat">
                                <i class="material-icons">chat</i>
                            </a>
                            <a class="tooltips" href="login.html" data-placement="top" data-original-title="Logout">
                                <i class="material-icons">input</i>
                            </a>
                    </div>
                </div>
            </li>
            <!-- End User Profile menu -->
                <li class="menu-heading">
                    <span>Main Menu</span>
                </li>
                <li class="nav-item start">
                    <a href="{{ route('admin.dashboard') }}"> <i class="material-icons">dashboard</i>  Dashboard
                        {{-- <span class="arrow "></span> --}}
                    </a>
                </li>

               {{--  <li class="nav-item {!! request()->is('admin/user*')? 'active open':'' !!}" >
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">assignment_turned_in</i>
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/user')? 'active':'' !!}" >
                            <a href="{{ route('admin.user.index') }}" class="nav-link ">
                                <span class="title">All Users</span>
                            </a>
                        </li>

                        <li class="nav-item {!! request()->is('admin/user/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.user.create') }}" class="nav-link ">
                                <span class="title">Add Users</span>
                            </a>
                        </li>
                    </ul>
                </li>
 --}}
                <li class="nav-item {!! request()->is('admin/reservation*')?' active open':'' !!}">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">business_center</i>
                        <span class="title">Reservation</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/reservation')? 'class="active"':'' !!}" >
                            <a href="{{ route('admin.reservation.index') }}" class="nav-link ">
                                <span class="title">Reservation List</span>
                            </a>
                        </li>
                        <li class="nav-item{!! request()->is('admin/reservation/create')? 'class="active"':'' !!}" >
                            <a href="{{ route('admin.reservation.create') }}" class="nav-link ">
                                <span class="title">Add Reservation</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {!! request()->is('admin/room*')? 'active open':'' !!}" >
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">vpn_key</i>
                        <span class="title">Rooms</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/room')? 'active':'' !!}" >
                            <a href="{{ route('admin.room.index') }}" class="nav-link ">
                                <span class="title">View All Rooms</span>
                            </a>
                        </li>

                        <li class="nav-item {!! request()->is('admin/room/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.room.create') }}" class="nav-link ">
                                <span class="title">Add New Room</span>
                            </a>
                        </li>
                    </ul>
                </li>
            
                <li class="nav-item {!! request()->is('admin/roomtypes*')? 'active open':'' !!}" >
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">assignment_turned_in</i>
                        <span class="title">Rooms Type</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/roomtypes')? 'active':'' !!}" >
                            <a href="{{ route('admin.roomtypes.index') }}" class="nav-link ">
                                <span class="title">   All Room Type</span>
                            </a>
                        </li>

                        <li class="nav-item {!! request()->is('admin/roomtypes/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.roomtypes.create') }}" class="nav-link ">
                                <span class="title">Add Room Type</span>
                            </a>
                        </li>
                    </ul>
                </li>

 
                <li class="nav-item {!! request()->is('admin/amenity*')? 'active open':'' !!}" >
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">assignment_turned_in</i>
                        <span class="title">Amenity</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/amenity')? 'active':'' !!}" >
                            <a href="{{ route('admin.amenity.index') }}" class="nav-link ">
                                <span class="title">   All amenity</span>
                            </a>
                        </li>

                        <li class="nav-item {!! request()->is('admin/amenity/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.amenity.create') }}" class="nav-link ">
                                <span class="title">Add amenity</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {!! request()->is('admin/slider*')? 'active open':'' !!}" >
 
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">collections</i>
                        <span class="title">Slider</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/slider')? 'active':'' !!}" >
                            <a href="{{ route('admin.slider.index') }}" class="nav-link ">
                                <span class="title">All Sliders</span>
                            </a>
                        </li>
                        <li class="nav-item {!! request()->is('admin/slider/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.slider.create') }}" class="nav-link ">
                                <span class="title">Add Slider</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">grade</i>
                        <span class="title">Services</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/service*')? 'active open':'' !!}" >
                            <a href="{{ route('admin.service.index') }}" class="nav-link ">
                                <span class="title">All Services</span>
                            </a>
                        </li>

                        <li class="nav-item {!! request()->is('admin/service/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.service.create') }}" class="nav-link ">
                                <span class="title">Add Service</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {!! request()->is('admin/addstaff*')? 'active open':'' !!}" >
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">people</i>
                        <span class="title">Staff</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/addstaff/create')? 'active':'' !!}" >
                            <a href="{{ route('admin.addstaff.create') }}" class="nav-link ">
                                <span class="title">Add Staff Details</span>
                            </a>
                        </li>
                        <li class="nav-item {!! request()->is('admin/addstaff')? 'active':'' !!}" >
                            <a href="{{ route('admin.addstaff.index') }}" class="nav-link ">
                                <span class="title">View All Staffs</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>

                 <li class="nav-item {!! request()->is('admin/inquiry*')?'class="active open"':'' !!}">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">business_center</i>
                        <span class="title">Inquiry</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item {!! request()->is('admin/inquiry*')? 'active':'' !!}" >
                            <a href="{{ route('admin.inquiry.index') }}" class="nav-link ">
                                <span class="title">Inquiry List</span>
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="{{ route('admin.inquiry.create') }}" class="nav-link ">
                                <span class="title">Add Inquiry</span>
                            </a>
                        </li> --}}
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>