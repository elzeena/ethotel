
<div class="form-group">
    <label for="name">Name</label>
    {!! Form::text('name',isset($inquiries->name)?$inquiries->name:'', ['class' => 'form-control px-3 py-3','placeholder'=>'Enter Name']) !!}
</div>


<div class="form-group">
    <label for="email">Email</label>
     {!! Form::text('email', isset($inquiries->email)?$inquiries->email:'', ['class' => 'form-control px-3 py-3']) !!}
</div>

<div class="form-group">
    <label for="subject">subject</label>
    {!! Form::text('subject', (isset($inquiries->subject)?$inquiries->subject:''), ['class' => 'form-control px-3 py-3', 'placeholder' => 'Enter Subject']) !!}
</div>
<div class="form-group">
    <label for="message">Message</label>
    {!! Form::textarea('message', isset($inquiries->message)?$inquiries->message:'', ['class' => 'form-control px-3 py-3','placeholder' => 'Enter message']) !!}
</div>


<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
