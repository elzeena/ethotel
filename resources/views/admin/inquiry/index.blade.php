@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Inquiry Informations</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                       @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-12">

              @include('admin.includes.flash_messages')

           		 <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header>All Inquiries</header>
                                     <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <div class="table table-striped custom-table table-hover">
                                        <table class="table table-hover table-checkable order-column full-width dataTable no-footer">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                {{-- <th>Actions</th> --}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($inquiries as $inquiry)
                                                <tr>
                                                    <td>{{ $inquiry->id }}</td>
                                                    <td>{{ $inquiry->name }}</td>
                                                    <td>{{ $inquiry->email }}</td>
                                                    <td>{{ $inquiry->subject}}</td>
                                                    <td>{{ $inquiry->message }}</td>
                                                    {{-- <td>
                                                        <a href="{{ route('admin.inquiry.edit', $inquiry->id) }}" class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a> --}}
                                                        {{-- <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> --}}
                                                        {{--<a href="{{ route('admin.room.delete',$room->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                            <i class="fasss fa-trash-o"></i>
                                                        </a>--}}
                                                        {{--        {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                               {!! Form::close() !!} --}}


                                                    {{-- </td> --}}

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
           	</div>
           </div>
            
        </div>
    </div>

@endsection