@extends('admin.includes.layout')

@section('extra_styles')

@endsection
@section ('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Bookings</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
        @include('admin.includes.breadcrumb_dashboard_link')
          <li class="active">Add Booking</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8">
        <div class="card card-box">
          <div class="card-head">
          <header>Add Booking</header>
        </div>
        <div class="card-body " id="bar-parent">
          {!! Form::open([
            'url' => route('admin.reservation.store'),
            'files'=>true,
            ])
          !!}          
          @include('admin.reservation.inc.form')
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <div class="col-lg-4">
         @include('admin.includes.flash_messages')
    </div>
  </div>
</div>
@endsection

@section('extra_scripts')
<script src="{{ asset('assets/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}" ></script>
<script src="{{ asset('assets/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js') }}" ></script>
<script type="text/javascript"></script>
@endsection