<div class="form-group">
    <label for="name">Select Room </label>
    {!! Form::select('reservations', $rooms, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="name">Name </label>
    {!! Form::text('name', isset($reservations->name)?$reservations->name:'', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="email">Email</label>
    {!! Form::text('email', isset($reservations->email)?$reservations->email:'', ['class' => 'form-control','placeholder'=>'Enter Email']) !!}
</div>
<div class="form-group">
    <label for="address">Address</label>
    {!! Form::text('address', isset($reservations->address)?$reservations->address:'', ['class' => 'form-control','placeholder'=>'Enter Address']) !!}
</div>
<div class="form-group">
    <label for="number">Phone No.</label>
    {!! Form::text('phone', isset($reservations->phone)?$reservations->phone:'', ['class' => 'form-control','placeholder'=>'Enter Phone No.']) !!}
</div>
{{-- <div class="form-group">
    <label for="arrival_date">Arrival Date</label>
    {!! Form::text('arrival_date', isset($reservations->arrival_date)?$reservations->arrival_date:'', ['class' => 'form-control','placeholder'=>'Enter Arrival Date'], ['id' => 'date']) !!}
</div> --}}
<div class="form-group">
    <label>Arrival Date</label>
         <input class="mdl-textfield__input input-group-addon form-control" type="text" id="date" data-dtp="dtp_eyhoJ" value="{{isset($reservations->arrival_date)?$reservations->arrival_date:''}}"  name="arrival_date">
 
</div>
<div class="form-group">
    <label>Departure Date</label>
         <input class="mdl-textfield__input input-group-addon form-control" type="text" id="date1" data-dtp="dtp_eyhoJ" value="{{isset($reservations->departure_date)?$reservations->departure_date:''}}"  name="departure_date">
 
</div>

 
{{-- <div class="form-group">
    <label for="departure_date">Departure Date</label>
    {!! Form::text('departure_date', isset($reservations->departure_date)?$reservations->departure_date:'', ['class' => 'form-control','placeholder'=>'Enter Departure Date'], ['id' => 'date']) !!}
</div> --}}
<div class="form-group">
    <label for="number" type="number">Number Adults</label>
    {!! Form::text('num_adults', isset($reservations->num_adults)?$reservations->num_adults:'', ['class' => 'form-control','placeholder'=>'Enter Adults']) !!}
</div>
<div class="form-group">
    <label for="number" type="number">Number Kids</label>
    {!! Form::text('num_kids', isset($reservations->num_kids)?$reservations->num_kids:'', ['class' => 'form-control','placeholder'=>'Enter Kids']) !!}
</div>
<div class="form-group">
    {!! Form::label('status', 'Status *', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
    {!! Form::select('gender', [1 =>'Male', 0 => 'Female'], null, ['class' => 'form-control']) !!}
</div>
{{--  <div class="form-group">
    {!! Form::label('image', 'Image', ['class' => 'form-group']) !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div> --}}
<div class="form-group">
    <label for="total_amt">Total Amount</label>
    {!! Form::text('total_amt', isset($reservations->total_amt)?$reservations->total_amt:'', ['class' => 'form-control','placeholder' => 'Enter Amount '])!!}
</div>
<div class="form-group">
    {!! Form::label('satus', 'Status', ['class'=>'form-label']) !!}
    {!! Form::select('status', [0=>'Inactive',1=>'Active'],0, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <div class="">
        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add Booking</button>
        <button type="reset" class="btn btn-danger"><i class="fa fa-recycle"></i> Cancel</button>
    </div>

</div>