@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Reservation Informations</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                       @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-12">

              @include('admin.includes.flash_messages')

           		 <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header>All Reservations</header>
                                     <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <div class="table table-striped custom-table table-hover">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Gender</th>
                                                {{-- <th>Title</th> --}}
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Arrival Date</th>
                                                <th>Departure Date</th>
                                                <th>Number Adults</th>
                                                <th>Number Kids</th>
                                               {{--  <th>Room Image</th> --}}
                                                <th>Status</th>
                                                <th>Total Amount</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                             @if(isset($reservations) && count($reservations)>0)
                                            @foreach($reservations as $reservation)
                                                <tr>
                                                    <td>{{ $reservation->id }}</td>
                                                    <td>{{ $reservation->name }}</td>
                                                    <td>{{ $reservation->email }}</td>
                                                    <td class="hidden-480">
                                                    {!! $reservation->genderone() !!}</td>
                                                    {{-- <td>{{ $reservation->rooms->title}}</td> --}}
                                                    <td>{{ $reservation->phone }}</td>
                                                    <td>{{ $reservation->address }}</td>
                                                    <td>{{ $reservation->arrival_date }}</td>
                                                    <td>{{ $reservation->departure_date }}</td>
                                                    <td>{{ $reservation->num_adults }}</td>
                                                    <td>{{ $reservation->num_kids }}</td>
                                                    {{-- <td>{{ $reservation->image }}</td> --}}
                                                    <td class="hidden-480">
                                                        
                                                    
                                                    <a href="#" class="hidden-480" id="clicked{{ $reservation->id }}">
                                                        {{ $reservation->status==1?'Active':'Inactive' }}
                                                     </a>
                                                    

                                                      <script type="text/javascript">

                                                        $('document').ready(function(){

                                                            $('#clicked{{ $reservation->id }}').on('click',function(e){
                                                                e.preventDefault();

                                                                var reservation_id = '{{ $reservation->id }}';

                                                                $('#clicked'+reservation_id).html('');


                                                            

                                                             var url = "{{  route('admin.reservation.publish',$reservation->id) }}";

                                                             $.ajax({
                                                                 type:'GET',
                                                                 url:url,
                                                                 dataType:'json',
                                                                 success:function(response){
                                                                    if(response.response.status==1){
                                                                        var $a = $('#clicked'+reservation_id);
                                                                        $a.append("Active");

                                                                    }else{
                                                                        var $a = $('#clicked'+reservation_id);
                                                                        $a.append("Inactive");
                                                                    }
                                                                 },

                                                              });
                                                            })
                                                             
                                                          });


                                                      </script>

                                                    </td>
                                                    <td>{{ $reservation->total_amt }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.reservation.edit', $reservation->id) }}" class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>

                                                         <a href="javascript:void(0);"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm btn-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> 

                                                         {!! Form::open(['id'=>'destroy-form','route' => ['admin.reservation.destroy', $reservation->id],'method'=>'DELETE']) !!}
                                                        {!! Form::close() !!}
                                                        {{-- <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> --}}
                                                        {{--<a href="{{ route('admin.room.delete',$room->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                            <i class="fasss fa-trash-o"></i>
                                                        </a>--}}
                                                        {{--        {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                               {!! Form::close() !!} --}}
                                                        <script type="text/javascript">
                                                        $(function(){
                                                            $(".btn-delete").on('click', function (event) {
                                                                event.preventDefault();
                                                            var $this = $(this);

                                                            swal({
                                                                title: "Are you sure?",
                                                                text: "Once deleted, you will not be able to recover this data!",
                                                                icon: "warning",
                                                                buttons: true,
                                                                dangerMode: true,
                                                            },
                                                            function (isConfirm) {
                                                            if (isConfirm) {
                                          
                                            
                                                                
                                                                location.href = $this.attr('href');
                                                                document.getElementById('destroy-form').submit();
                                                                swal("Poof! Your data has been deleted!", {
                                                                icon: "success",
                                                            });
                                                        } else {
                                                            swal("Your data is safe!");
                                                        }
                                                        });
                                                    });
                                                    });
                                                        </script>

                                                        {{-- <a href="javascript:void(0);"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm btn-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>  --}}


                                                {{-- <a href="{{ route('admin.roomtypes.delete',$types->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                    <i class="fa fa-trash-o"></i>
                                                </a> --}}


                                                      {{-- {!! Form::open(['id'=>'destroy-form','route' => ['admin.reservation.destroy', $reservation->id],'method'=>'DELETE']) !!}
                                                        {!! Form::close() !!} --}}

                                                        
                                                        {{-- <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> --}}
                                                        {{--<a href="{{ route('admin.room.delete',$room->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                            <i class="fasss fa-trash-o"></i>
                                                        </a>--}}
                                                        {{--        {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                               {!! Form::close() !!} --}}


                                                    </td>

                                                </tr>
                                            @endforeach
                                            @else

                                            <tr>
                                              <td colspan="5">No Data Found</td>
                                            </tr>
                                            
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
           	</div>
           </div>
            
        </div>
    </div>

@endsection

@section('extra_styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.css') }}">
@endsection

@section('extra_scripts')
<script src="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.js') }}" ></script>

</script>

  @endsection

