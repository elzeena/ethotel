<div class="form-group">
    <label for="room_code">Room Code </label>
        {!! Form::text('room_code',isset($rooms->room_code)?$rooms->room_code:'', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="excerpt">Short Description</label>
    {!! Form::text('excerpt',isset($rooms->excerpt)?$rooms->excerpt:'', ['class' => 'form-control','placeholder'=>'Short description']) !!}
</div>

<div class="form-group">
    <label for="room_type_id">Room Type</label>
   {!! Form::select('room_type_id',$room_types, isset($rooms)?$rooms->room_types()->get()->toArray():null, ['class' => 'form-control']) !!}

    </div>

<div class="form-group">
    {!! Form::label('image', 'Image', ['class' => 'form-group']) !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
@if(isset($rooms))
<div class="form-group">
    {!! Form::label('existing_image', 'Existing logo', ['class' => 'form-group']) !!}
    <div class="form-control">
        {!! $rooms->renderImage([
                    'class'=>'img-thumbnail',
                 ]) !!}
    </div>
</div>
@endif

<div class="form-group">
    <label for="description">Description</label>
     {!! Form::textarea('description',isset($rooms->description)?$rooms->description:'', ['class' => 'form-control textarea']) !!}
</div>

<div class="form-group">
    <label for="regular_price">Regular Price</label>
    {!! Form::text('regular_price', (isset($rooms)? $rooms->regular_price:''), ['class' => 'form-control', 'placeholder' => 'Enter Regular Price']) !!}
</div>
<div class="form-group">
    <label for="sell_price">Selling Price</label>
    {!! Form::text('sell_price',isset($rooms->sell_price)?$rooms->sell_price:'', ['class' => 'form-control','placeholder' => 'Enter Selling Price']) !!}
</div>

<div class="form-group">
    <label for="no_bed">Number of Beds</label>
    {!! Form::text('no_bed',isset($rooms->no_bed)?$rooms->no_bed:'', ['class' => 'form-control','placeholder' => 'Enter Num of beds '])!!}
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
