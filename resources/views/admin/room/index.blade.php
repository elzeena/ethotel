@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Room Informations</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                       @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-12">

              @include('admin.includes.flash_messages')

           		 <div class="card card-box">
                                <div class="card-head">
                                    <header>Add Rooms</header>
                                     <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <div class="table-scrollable">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Room Code</th>
                                                <th>Room Type</th>
                                                <th>Short Description</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($rooms as $room)
                                                <tr>
                                                    <td>{{ $room->id }}</td>
                                                    <td>{{ $room->room_code }}</td>
                                                    <td>{{ $room->room_types->title }}</td>

                                                    <td>{{ $room->excerpt }}</td>
                                                    <td>
                                                        <a href="{{ route('admin.room.edit', $room->id) }}" class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>

                                                        <a href="javascript:void(0);"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm btn-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> 

                                                         {!! Form::open(['id'=>'destroy-form','route' => ['admin.room.destroy', $room->id],'method'=>'DELETE']) !!}
                                                        {!! Form::close() !!}
                                                        {{-- <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> --}}
                                                        {{--<a href="{{ route('admin.room.delete',$room->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                            <i class="fasss fa-trash-o"></i>
                                                        </a>--}}
                                                        {{--        {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                               {!! Form::close() !!} --}}
                                                        <script type="text/javascript">
                                                        $(function(){
                                                            $(".btn-delete").on('click', function (event) {
                                                                event.preventDefault();
                                                            var $this = $(this);

                                                            swal({
                                                                title: "Are you sure?",
                                                                text: "Once deleted, you will not be able to recover this data!",
                                                                icon: "warning",
                                                                buttons: true,
                                                                dangerMode: true,
                                                            },
                                                            function (isConfirm) {
                                                            if (isConfirm) {
                                          
                                            
                                                                
                                                                location.href = $this.attr('href');
                                                                document.getElementById('destroy-form').submit();
                                                                swal("Poof! Your data has been deleted!", {
                                                                icon: "success",
                                                            });
                                                        } else {
                                                            swal("Your data is safe!");
                                                        }
                                                        });
                                                    });
                                                    });
                                                        </script>

                                                    </td>

                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
           	</div>
           </div>
            
        </div>
    </div>



@endsection

@section('extra_styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.css') }}">
@endsection

@section('extra_scripts')
<script src="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.js') }}" ></script>

</script>

  @endsection

