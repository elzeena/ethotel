
@extends('admin.includes.layout')

@section ('content')
      <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">RoomTypes</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                @include('admin.includes.breadcrumb_dashboard_link')
                                <li><a class="parent-item" href="#">RoomTypes</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Edit RoomType Details</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
            <div class="col-sm-12">
<div class="card-box">
                <div class="card-head">
                  <header>Edit RoomType Details</header>
                  <button id = "panel-button" 
                                 class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                 data-upgraded = ",MaterialButton">
                                 <i class = "material-icons">more_vert</i>
                              </button>
                              <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                 data-mdl-for = "panel-button">
                                 <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                 <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                 <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                              </ul>
                </div>
                <div class="card-body row">
                         {!! Form::open(array(
                            'url' => route('admin.roomtypes.update', $room_types->id),
                            'files' => true,
                            'role' =>'form',
                            'method'=> 'PUT',
                            ))
                            !!}
          
                          @include('admin.roomtypes.inc.form')
                          {{ Form::close() }}
              </div>
            </div>
          </div> 
                </div>
      </div>

@endsection