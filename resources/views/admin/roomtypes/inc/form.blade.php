<div class="form-group">
    <label for="title">Title</label>
        {!! Form::text('title',isset($room_types->title)?$room_types->title:'', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="description">Description</label>
     {!! Form::textarea('description',isset($room_types->description)?$room_types->description:'', ['class' => 'form-control textarea']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
