
@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">

            @include('admin.includes.flash_messages')

            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Dashboard</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-6">
                <div class="card card-topline-green">
                                        <div class="card-head">
                                            <header>ROOMS AVAILABLE</header>
                                            <div class="tools">
                                                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
			                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
			                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                            </div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="table-scrollable">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>SN</th>
                                                            <th id="rom_type">Room Type</th>
                                                            <th>Description</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    	@foreach($room_types['rows'] as $types)
                                                        <tr>
                                                            <td>{{ $types->id }}</td>
                                                            <td id="rom_type">{{ $types->title  }}</td>
                                                            <td>{{ $types->description }}</td>
                                                    <td>
                                                     <a href="{{ route('admin.roomtypes.edit', $types->id) }}" class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        

                                                        <a href="javascript:void(0);"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm btn-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> 


                                                {{-- <a href="{{ route('admin.roomtypes.delete',$types->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                    <i class="fa fa-trash-o"></i>
                                                </a> --}}


                                                      {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                        {!! Form::close() !!}

                            <script type="text/javascript">
                                $(function(){
                                    $(".btn-delete").on('click', function (event) {
                                        event.preventDefault();
                                    var $this = $(this);

                                    swal({
                                        title: "Are you sure?",
                                        text: "Once deleted, you will not be able to recover this data!",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true,
                                    },
                                    function (isConfirm) {
                                    if (isConfirm) {
                                          
                                            
                                            
                                            location.href = $this.attr('href');
                                            document.getElementById('destroy-form').submit();
                                            swal("Poof! Your data has been deleted!", {
                                            icon: "success",
                                        });
                                    } else {
                                        swal("Your data is safe!");
                                    }
                                    });
                                });
                                });
                                                        </script>


                                                    
                                                    </td>
                                                        
                                                        </tr>
                                                        @endforeach
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
           	</div>
           </div>
            
        </div>
    </div>

@endsection

@section('extra_styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.css') }}">
@endsection
@section('extra_scripts')
<script src="{{ asset('assets/admin/plugins/sweet-alert/sweetalert.min.js') }}" ></script>
<script>
    // $("#destroy-form").on('submit',function(e){
    //     e.preventDefault();
    //     alert('clicked');
    // });
    


</script>

<script>
    
</script>

<script>
  $( function() {
    var availableTags = [
      "Deluxe",
      "SuperDeluxe",
      "King",
      "General",
      "Special"
    ];
    $( "#rom_type" ).autocomplete({
      source: availableTags
    });
  } );
  </script>

@endsection


