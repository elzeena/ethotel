@extends('admin.includes.layout')
@section ('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Dashboard</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          @include('admin.includes.breadcrumb_dashboard_link')
          <li class="active">Dashboard</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8">
        <div class="card card-box">
          <div class="card-head">
          <header>Add Service</header>
        </div>
        <div class="card-body " id="bar-parent">
          {!! Form::open([
            'url' => route('admin.service.store'),
            'files'=>true,
            ])
          !!}          
          @include('admin.service.inc.form')
          {{ Form::close() }}
        </div>
      </div>
    </div>
    <div class="col-lg-4">
         @include('admin.includes.flash_messages')
    </div>
  </div>
</div>
@endsection