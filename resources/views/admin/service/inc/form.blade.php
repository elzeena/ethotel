<div class="form-group">
    <label for="title">Title </label>
    <input type="text" class="form-control" value="{{(isset($data['row']) && $data['row']->title)? $data['row']->title:old('title') }}" name="title" placeholder="Enter Title" >
</div>
<div class="form-group">
    <label for="short_description">Short Description</label>
    <textarea name="short_description" class="form-control" placeholder="Enter Short Description To appear on HomePage" >
        {{(isset($data['row']) &&$data['row']->short_description)? $data['row']->short_description:old('short_description') }}
    </textarea>
</div>
<div class="form-group">
    <label for="rank">Position</label>
    <input type="numeric" name="rank" value="{{(isset($data['row']) && $data['row']->rank)? $data['row']->rank:old('rank') }}" class="form-control" placeholder="Enter Postion" >
</div>
<div class="form-group">
    <label for="content">Content</label>
    <textarea name="content" class="form-control" placeholder="Enter Content" >
    {{(isset($data['row']) && $data['row']->content)? $data['row']->content:old('content') }}
    </textarea>
</div>
<div class="form-group">
    <label for="image">Featured Photo</label>
    <input type="file" name="image" class="form-control">
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>

<div class="form-group">
    <button type="button"  onclick="location.href='{{ route('admin.service.index') }}'"class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
</div>