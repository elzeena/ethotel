@extends('admin.includes.layout')
@section ('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Services</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                       @include('admin.includes.breadcrumb_dashboard_link')
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
           <div class="row">
           	<div class="col-sm-12">

              @include('admin.includes.flash_messages')

           		 <div class="card card-box">
                                <div class="card-head">
                                    <header>List Of Services</header>
                                     <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <div class="table-scrollable">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>Title</th>
                                                <th>Short Description</th>
                                                <th>Featured Image</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                           @if(isset($data['services']) && count($data['services'])>0)
                                            @foreach($data['services'] as $service)
                                                <tr>
                                                    <td>{{ $service->id }}</td>
                                                    <td>{{ $service->title }}</td>
                                                    <td>{{ $service->short_description }}</td>
                                                    <td>
                                                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <a href="#">
                                                          <img src="{{ asset($service->service_img) }}" alt="" style="max-width: 100px;height: auto;">
                                                        </a>
                                                      </div>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.service.edit', $service->id) }}" class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        {{-- <a href="#"  onclick="document.querySelector('#destroy-form').submit();return false;"
                                                           class="btn btn-tbl-delete btn-xs bootbox-confirm">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a> --}}
                                                        {{--<a href="{{ route('admin.room.delete',$room->id) }}" class="btn btn-tbl-edit btn-xs" id="btn-delete">
                                                            <i class="fasss fa-trash-o"></i>
                                                        </a>--}}
                                                        {{--        {!! Form::open(['id'=>'destroy-form','route' => ['admin.roomtypes.destroy', $types->id],'method'=>'DELETE']) !!}
                                                               {!! Form::close() !!} --}}


                                                    </td>

                                                </tr>
                                            @endforeach

                                           @else
                                           <tr>
                                             <td colspan="5">No Data Found</td>
                                           </tr>
                                           @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
           	</div>
           </div>
            
        </div>
    </div>

@endsection