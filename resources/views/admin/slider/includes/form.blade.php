<div class="form-group">
    {!! Form::label('title','Title',['class'=>'form-label']) !!}
    {!! Form::text('title',isset($slider->title)?$slider->title:'' , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('image','Photo',['class'=>'form-label']) !!}
    {!! Form::file('image', ['class'=>'form-control']) !!}
</div>

@if(isset($slider))
<div class="form-group">
    {!! Form::label('existing_image', 'Existing logo', ['class' => 'form-group']) !!}
    <div class="form-control">
        {!! $slider->renderImage(['class'=>'img-thumbnail']) !!}
    </div>
</div>
@endif

<div class="form-group">
    {!! Form::label('links','Link',['class'=>'form-label']) !!}
    {!! Form::text('links',isset($slider->links)?$slider->links:'' , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('caption','Caption',['class'=>'form-label']) !!}
    {!! Form::text('caption',isset($slider->caption)?$slider->caption:'' , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('rank','Position',['class'=>'form-label']) !!}
    {!! Form::text('rank',isset($slider->rank)?$slider->rank:'' , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('satus', 'Status', ['class'=>'form-label']) !!}
    {!! Form::select('status',[0=>'Inactive',1=>'Active'],(isset($slider) && $slider->status)? $slider->status:null, ['class'=>'form-control']) !!}
</div>

