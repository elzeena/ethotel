@extends('admin.includes.layout')

@section('title')

User::Profile
@endsection

@section('content')

{!! Form::open([
            'url' => route('admin.profile.update'),
            'files'=>true,
            'method' =>'POST',
            ])
!!}
	<div class="page-content-wrapper">

                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Profile Manager</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                @include('admin.includes.breadcrumb_dashboard_link')
                                <li><a class="parent-item" href="{{ route('admin.profile')}}">Profile</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Update</li>
                            </ol>
                        </div>
                    </div>
                     <div class="row">
							<div class="col-sm-12">
							@include('admin.includes.flash_messages')

								<div class="card-box">
									<div class="card-head">
										<header>Basic Information</header>
										<button id = "panel-button" 
				                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
				                           data-upgraded = ",MaterialButton">
				                           <i class = "material-icons">more_vert</i>
				                        </button>
				                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
				                           data-mdl-for = "panel-button">
				                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
				                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
				                        </ul>
									</div>
									<div class="card-body row">
							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "name"
						                     value="{{ $data['user']->name }}" type = "text">
						                     <label for = "name" class = "mdl-textfield__label {{ $errors->has('name') ? ' is-invalid' : '' }}">User Name</label>

						                     @if ($errors->has('name'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('name') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div>

							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "email" value="{{ $data['user']->email }}" type = "">
						                     <label for = "email" class = "mdl-textfield__label {{ $errors->has('email') ? ' is-invalid' : '' }}">Email</label>

						                     @if ($errors->has('email'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('email') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 

							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "first_name" value="{{ $data['user']->first_name }}" type = "text">
						                     <label for = "first_name" class = "mdl-textfield__label {{ $errors->has('first_name') ? ' is-invalid' : '' }}">First Name</label>

						                     @if ($errors->has('first_name'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('first_name') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 

							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "middle_name" value="{{ $data['user']->middle_name }}" type = "text">
						                     <label for = "middle_name" class = "mdl-textfield__label {{ $errors->has('middle_name') ? ' is-invalid' : '' }}">Middle Name</label>

						                     @if ($errors->has('middle_name'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('middle_name') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 



							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "last_name" value="{{ $data['user']->last_name }}" type = "text">
						                     <label for = "last_name" class = "mdl-textfield__label {{ $errors->has('last_name') ? ' is-invalid' : '' }} ">Last Name</label>

						                     @if ($errors->has('last_name'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('last_name') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 


							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "contact" value="{{ $data['user']->contact }}" type = "text">
						                     <label for = "contact" class = "mdl-textfield__label {{ $errors->has('contact') ? ' is-invalid' : '' }}">Contact</label>

						                     @if ($errors->has('contact'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('contact') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 


							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <textarea name="address" id="" cols="30" rows="10" class="form-control">{{ $data['user']->address }}</textarea>
						                     <label for = "address" class = "mdl-textfield__label {{ $errors->has('address') ? ' is-invalid' : '' }}">Address</label>

						                     @if ($errors->has('contact'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('address') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 


							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "password" type = "">
						                     <label for = "password" class = "mdl-textfield__label {{ $errors->has('password') ? ' is-invalid' : '' }}">Password</label>

						                     @if ($errors->has('password'))
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $errors->first('password') }}</strong>
			                                    </span>
                                			@endif

						                  </div>
							            </div> 

							            <div class="col-lg-6 p-t-20"> 
							              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						                     <input class = "mdl-textfield__input" name = "password_confirmation" type = "">
						                     <label for = "password_confirmation" class = "mdl-textfield__label">Confirm New Password</label>
						                  </div>
							            </div>
							        
								         <div class="col-lg-12 p-t-20 text-center"> 
							              	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Update</button>
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
							            </div>
									</div>
								</div>
							</div>
						</div> 
                </div>
            </div>

{{ Form::close() }}
@endsection