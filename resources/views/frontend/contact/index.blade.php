@extends('frontend.layouts.master')


@section('content')


  <div class="block-30 block-30-sm item" style="background-image: url({{ asset('assets/frontend/images/bg_2.jpg') }});" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-10">
          <span class="subheading-sm">Contact</span>
          <h2 class="heading">Get In Touch</h2>
        </div>
      </div>
    </div>
  </div>
<div class="site-section bg-light">
  

  <div class="site-section">
    <div class="container">
      <div class="row block-9">
        <div class="col-sm-12">
          @include('admin.includes.flash_messages')
        </div>
        <div class="col-md-6 pr-md-5">

           {!! Form::open([
            'url' => route('submitContactForm'),
            'id' => 'myForm',
            'files'=>true,
            'method'=>'POST',
            'onsubmit' =>'return validateForm()',
            ])
          !!}          
          

<div class="form-group">
    <label for="name">Name</label>
    {!! Form::text('name',isset($inquiry->name)?$inquiry->name:'', ['class' => 'form-control px-3 py-3','placeholder'=>'Enter Name']) !!}
</div>


<div class="form-group">
    <label for="email">Email</label>
     {!! Form::text('email', isset($inquiry->email)?$inquiry->email:'', ['class' => 'form-control px-3 py-3', 'placeholder' => 'Enter Email']) !!}
</div>

<div class="form-group">
    <label for="subject">Subject</label>
    {!! Form::text('subject', (isset($inquiry->subject)?$inquiry->subject:''), ['class' => 'form-control px-3 py-3', 'placeholder' => 'Enter Subject']) !!}
</div>
<div class="form-group">
    <label for="message">Message</label>
    {!! Form::textarea('message', isset($inquiry->message)?$inquiry->message:'', ['class' => 'form-control px-3 py-3','placeholder' => 'Enter message']) !!}
</div>

<div class="form-group">
    <button type="submit" id="submit" class="btn btn-primary py-3 px-5">Submit</button>
</div>

          {{ Form::close() }}
          {{-- <form action="{{ route('submitContactForm') }} method = "POST">
            <div class="form-group">
              <input type="text" class="form-control px-3 py-3" placeholder="Your Name">
            </div>
            <div class="form-group">
              <input type="email" class="form-control px-3 py-3" placeholder="Your Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control px-3 py-3" placeholder="Subject">
            </div>
            <div class="form-group">
              <textarea name="" id="" cols="30" rows="7" class="form-control px-3 py-3" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
            </div>
          </form> --}}
        
        </div>

        <div class="col-md-6" id="map"></div>
      </div>
    </div>
  </div>

    <section class="page-title">
        <div class="container">
            <h3 class="text-center">Contact Us</h3>
        </div><!-- /.container -->
    </section><!-- /.page-title -->
    <section class="breadcumb">
        <div class="container">
            <ul class="list-inline text-center">
                <li><a href="#">Home</a></li>
                <li><span>Contact Us</span></li>
            </ul><!-- /.list-inline -->
        </div><!-- /.container -->
    </section><!-- /.breadcumb -->

    <div
            class="google-map"
            id="home-google-map"
            data-map-lat="40.686767"
            data-map-lng="-73.373455"
            data-icon-path="img/map-marker.png"
            data-map-title="Brooklyn, New York, United Kingdom"
            data-map-zoom="11"
            data-markers='{
    "marker-1": [40.686767, -73.373455, "<h4>Main Office</h4><p>Babylon Branch , Lindenhurst, UK</p>"],
    "marker-2": [40.713990, -73.559016, "<h4>Branch Office</h4> <p>291 Park Ave S, East Meadow, UK</p>"],
    "marker-3": [40.664270, -73.708464, "<h4>Branch Office</h4> <p>230 Park Ave S, Vally Stream, UK</p>"],
    "marker-4": [40.663712, -73.500526, "<h4>Branch Office</h4> <p>230 Park Ave S, Sea Ford, UK</p>"]
}'>


    </div>

    <section class="contact-section sec-pad">
        <div class="container">
            <div class="col-md-4">
                <div class="title">
                    <h3>Contact Info</h3>
                    <ul class="info-text">
                        <li>
                            <div class="img-box">
                                <div class="inner-box">
                                    <i class="fa fa-map-marker"></i>
                                </div><!-- /.inner-box -->
                            </div><!-- /.img-box -->
                            <div class="text-box">
                                <h4>Lacation</h4>
                                <p>PO Box 16122 Collins Street <br /> West Victoria 8007 Canada</p>
                            </div><!-- /.text-box -->
                        </li>
                        <li>
                            <div class="img-box">
                                <div class="inner-box">
                                    <i class="fa fa-phone"></i>
                                </div><!-- /.inner-box -->
                            </div><!-- /.img-box -->
                            <div class="text-box">
                                <h4>Phone Number</h4>
                                <p>(+48) 564-334-21-22-34 <br />(+48) 564-334-21-22-38</p>
                            </div><!-- /.text-box -->
                        </li>
                        <li>
                            <div class="img-box">
                                <div class="inner-box">
                                    <i class="fa fa-envelope"></i>
                                </div><!-- /.inner-box -->
                            </div><!-- /.img-box -->
                            <div class="text-box">
                                <h4>Email</h4>
                                <p>info@templatepath.com <br />info@gardenors.com</p>
                            </div><!-- /.text-box -->
                        </li>
                    </ul><!-- /.info-text -->
                </div><!-- /.title -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div class="title">
                    <h3>Get In Touch</h3>
                </div><!-- /.title -->
                <form action="#" class="contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="Name" />
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <input type="text" placeholder="Email" />
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-12">
                            <input type="text" placeholder="Phone" />
                        </div><!-- /.col-md-12 -->
                        <div class="col-md-12">
                            <textarea placeholder="Your Message"></textarea>
                            <button class="thm-btn" type="submit">Send Message</button>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </form>
            </div><!-- /.col-md-8 -->
        </div><!-- /.container -->
    </section><!-- /.contact-section -->


    <section class="subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Subscribe to our Newsletter</h3>
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <form action="#">
                        <input type="text" placeholder="Your Email" />
                        <button type="submit">Subscribe</button>
                    </form>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.subscribe-section -->
@endsection
<script type="text/javascript">
 
   function validateForm() {
    
    if (document.forms["myForm"]["name"].value == "") {
        alert("Enter your name");
        return false;
    }
    if (document.forms["myForm"]["email"].value == "") {
        alert("Enter your email");
        return false;
    }
    if (document.forms["myForm"]["subject"].value == "") {
        alert("Enter subject");
        return false;
    }if (document.forms["myForm"]["message"].value == "") {
        alert("Enter message");
        return false;
    }


}

   
 
</script>




@section('scripts')
@endsection


