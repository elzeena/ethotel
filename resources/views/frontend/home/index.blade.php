@extends('frontend.layouts.master')

@section('content')
    @if($data['slider'])
    <section class="rev_slider_wrapper">
        <div class="rev_slider slider1" id="slider1"  data-version="5.0" data-height="750">
            <ul>

                @foreach ($data['slider'] as $slider)
                <li data-transition="random">
                    <img src="{{ asset($slider->image) }}"  alt="" width="1920" height="750" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                    <div class="tp-caption tp-resizeme banner-caption-h3 ttu"
                         data-x="left" data-hoffset="0"
                         data-y="top" data-voffset="270"
                         data-transform_idle="o:1;"
                         data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                         data-splitin="none"
                         data-splitout="none"
                         data-start="500">
                        {{ $slider->title }}
                    </div>
                    <div class="tp-caption tp-resizeme banner-caption-p text-center"
                         data-x="left" data-hoffset="0"
                         data-y="top" data-voffset="370"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-start="1500">
                        {{ $slider->caption }}
                    </div>

                    <div class="tp-caption tp-resizeme"
                         data-x="left" data-hoffset="0"
                         data-y="top" data-voffset="425"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-start="2000">
                        <a href="{{ route('contact') }}" class="thm-btn">Contact Now</a> &emsp;
                    </div>

                </li>
                @endforeach

            </ul>
        </div>
    </section>
    @endif

    <section class="book-now-section">
        <div class="container">
            <div class="title-box">
                <h3><span>Book</span> Your Room</h3>
                <i class="fa fa-check"></i>
            </div><!-- /.title-box -->
            <div class="form-grp">
                <input type="text" placeholder="Araival date" class="datepicker" />
                <i class="fa fa-calendar"></i>
            </div><!-- /.form-grp -->
            <div class="form-grp">
                <input type="text" placeholder="Departure date" class="datepicker" />
                <i class="fa fa-calendar"></i>
            </div><!-- /.form-grp -->
            <div class="form-grp">
                <select class="selectpicker">
                    <option value="">Adults</option>
                    <option value="">Adults</option>
                    <option value="">Adults</option>
                    <option value="">Adults</option>
                </select>
            </div><!-- /.form-grp -->
            <div class="form-grp">
                <select class="selectpicker">
                    <option value="">Children</option>
                    <option value="">Children</option>
                    <option value="">Children</option>
                    <option value="">Children</option>
                </select>
            </div><!-- /.form-grp -->
            <div class="form-grp">
                <button class="thm-btn" type="submit">Check Availability</button>
            </div><!-- /.form-grp -->
        </div><!-- /.container -->
    </section><!-- /.book-now-section -->

    @foreach($data['abouts'] as $aboutus)
    <section class="welcome-section sec-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="welcome-left">
                        <div class="sec-title">
                            <h1>welcome</h1>
                            <h2>Crowny Hotel</h2>
                        </div><!-- /.sec-title -->
                        <p>{{ $aboutus->details }}</p>
                        <a href="{{ route('about-us') }}" class="thm-btn">About Us</a>
                    </div><!-- /.welcome-left -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <img src="{{ asset('assets/web/img/welcome.png') }}" alt="Awesome Image"/>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.about-section -->
    @endforeach
    <section class="counter-section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter text-center">
                        <h4 class="counter">2500</h4>
                        <span class="decor-line"></span>
                        <p>Customers</p>
                    </div><!-- /.single-counter -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter text-center">
                        <h4 class="counter">1250</h4>
                        <span class="decor-line"></span>
                        <p>Happy Coustomers</p>
                    </div><!-- /.single-counter -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter text-center">
                        <h4 class="counter">150</h4>
                        <span class="decor-line"></span>
                        <p>Expert Technicians</p>
                    </div><!-- /.single-counter -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-6">
                    <div class="single-counter text-center">
                        <h4 class="counter">3550</h4>
                        <span class="decor-line"></span>
                        <p>Desktop Repaired</p>
                    </div><!-- /.single-counter -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.counter-section -->

    <section class="our-rooms sec-pad">
        <div class="container">
            <div class="sec-title">
                <h1>Explore</h1>
                <h2>Our Rooms</h2>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt </p>
            </div><!-- /.sec-title -->
            <div class="room-carousel owl-carousel owl-theme">
                @foreach ($data['rooms'] as $room)
                <div class="item">
                    <div class="single-room">
                        <div class="img-box">
                            <a href="{!! route('rooms.title',$room->id,$room->room_types->title) !!}"><img src="{{ asset($room->image) }}" alt="Image placeholder"></a>
                        </div><!-- /.img-box -->
                        <div class="bottom-box clearfix">
                            <h3>{{$room->room_types->title}}</h3>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.stars -->
                            <p>{{$room->excerpt}}</p>
                            <a href="{!! route('rooms.title',$room->id,$room->room_types->title) !!}" class="thm-btn">book now</a>
                            <div class="price-box">
                                <span class="price">{{$room->regular_price}}</span><br />
                                <span>per night</span>
                            </div><!-- /.price-box -->
                        </div><!-- /.bottom-box -->
                    </div><!-- /.single-room -->
                </div><!-- /.item -->
                @endforeach

            </div><!-- /.room-carousel owl-carousel owl-theme -->
        </div><!-- /.container -->
    </section><!-- /.our-room -->

    <section class="gallery-section gray-bg sec-pad">
        <div class="container">
            <div class="sec-title">
                <h1>Photos</h1>
                <h2>Our Gallery</h2>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt </p>
            </div><!-- /.sec-title -->
        </div><!-- /.container -->
        <div class="gallery-carousel owl-carousel owl-theme">
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-1.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-1.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-2.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-2.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-3.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-3.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-4.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-4.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-5.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-5.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-gallery">
                    <div class="img-box">
                        <img src="{{ asset('assets/web/img/gallery-6.png') }}" alt="Awesome Image"/>
                        <div class="overlay">
                            <div class="dt">
                                <div class="dtc">
                                    <div class="box">
                                        <a data-group="1" href="{{ asset('assets/web/img/gallery-6.png') }}" class="img-popup"><i class="fa fa-plus"></i></a>
                                        <h3>Photo title here</h3>
                                    </div><!-- /.box -->
                                </div><!-- /.dtc -->
                            </div><!-- /.dt -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-gallery -->
            </div><!-- /.item -->
        </div><!-- /.gallery-carousel -->
    </section><!-- /.gallery-section -->

    <section class="our-services sec-pad">
        <div class="container">
            <div class="sec-title">
                <h1>Services</h1>
                <h2>Our Services</h2>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt </p>
            </div><!-- /.sec-title -->
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="single-service">
                                <i class="icon-food"></i>
                                <h3>Delious Food</h3>
                            </div><!-- /.single-service -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <div class="single-service">
                                <i class="icon-people-1"></i>
                                <h3>Fitness Gym</h3>
                            </div><!-- /.single-service -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <div class="single-service">
                                <i class="icon-restaurant"></i>
                                <h3>Inhouse Restaurant</h3>
                            </div><!-- /.single-service -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <div class="single-service">
                                <i class="icon-people"></i>
                                <h3>Beauty Spa</h3>
                            </div><!-- /.single-service -->
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <img src="{{ asset('assets/web/img/service.png') }}" alt="Awesome Image"/>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.our-services -->

    <section class="tesimonial-section sec-pad">
        <div class="container">
            <div class="testimonial-carousel owl-carousel owl-theme">
                <div class="item">
                    <div class="single-testimonial-carousel">
                        <i class="fa fa-quote-right"></i>
                        <p>Lorem ipsum dolor sit amet constur adipisicing elit, sed do eiusmtempor incid et dolore magna aliqu enim ad minim veniam quis nostrud exercittion ullamco laboris nisi ut aliquip excepteur sint occaecat cuidatat non proident sunt in culpa qui officia. deserunt mollit anim id est laborum.</p>
                        <h3>Julia Robertson</h3>
                        <span>Julia Robertson</span>
                    </div><!-- /.single-testimonial-carousel -->
                </div><!-- /.item -->
                <div class="item">
                    <div class="single-testimonial-carousel">
                        <i class="fa fa-quote-right"></i>
                        <p>Lorem ipsum dolor sit amet constur adipisicing elit, sed do eiusmtempor incid et dolore magna aliqu enim ad minim veniam quis nostrud exercittion ullamco laboris nisi ut aliquip excepteur sint occaecat cuidatat non proident sunt in culpa qui officia. deserunt mollit anim id est laborum.</p>
                        <h3>Julia Robertson</h3>
                        <span>Julia Robertson</span>
                    </div><!-- /.single-testimonial-carousel -->
                </div><!-- /.item -->
                <div class="item">
                    <div class="single-testimonial-carousel">
                        <i class="fa fa-quote-right"></i>
                        <p>Lorem ipsum dolor sit amet constur adipisicing elit, sed do eiusmtempor incid et dolore magna aliqu enim ad minim veniam quis nostrud exercittion ullamco laboris nisi ut aliquip excepteur sint occaecat cuidatat non proident sunt in culpa qui officia. deserunt mollit anim id est laborum.</p>
                        <h3>Julia Robertson</h3>
                        <span>Julia Robertson</span>
                    </div><!-- /.single-testimonial-carousel -->
                </div><!-- /.item -->
            </div><!-- /.testimonial-carousel owl-carousel owl-theme -->
        </div><!-- /.container -->
    </section><!-- /.tesimonial-section -->

    <section class="our-news sec-pad">
        <div class="container">
            <div class="sec-title">
                <h1>News</h1>
                <h2>Our News</h2>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt </p>
            </div><!-- /.sec-title -->
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="single-blog-post">
                        <div class="img-box">
                            <img src="{{ asset('assets/web/img/blog-1.png')}}" alt="Awesome Image"/>
                            <div class="overlay">
                                <div class="dt">
                                    <div class="dtc">
                                        <div class="date-box">
                                            <span>20</span>May
                                        </div><!-- /.date-box -->
                                    </div><!-- /.dtc -->
                                </div><!-- /.dt -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                        <div class="bottom-box">
                            <h3><a href="#">Finibus bonorum malorum.</a></h3>
                            <ul class="meta">
                                <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                <li><a href="#"><i class="fa fa-heart-o"></i> 350</a></li>
                                <li><a href="#"><i class="fa fa-comments-o"></i> 30</a></li>
                            </ul><!-- /.meta -->
                            <p>Lorem ipsum dolor amet consectetur adipisicing elit sed eiusm tempor incididunt ut labore dolor magna aliqua.</p>
                        </div><!-- /.bottom-box -->
                    </div><!-- /.single-blog-post -->
                </div><!-- /.col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-blog-post">
                        <div class="img-box">
                            <img src="{{ asset('assets/web/img/blog-2.png')}}" alt="Awesome Image"/>
                            <div class="overlay">
                                <div class="dt">
                                    <div class="dtc">
                                        <div class="date-box">
                                            <span>20</span>May
                                        </div><!-- /.date-box -->
                                    </div><!-- /.dtc -->
                                </div><!-- /.dt -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                        <div class="bottom-box">
                            <h3><a href="#">Finibus bonorum malorum.</a></h3>
                            <ul class="meta">
                                <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                <li><a href="#"><i class="fa fa-heart-o"></i> 350</a></li>
                                <li><a href="#"><i class="fa fa-comments-o"></i> 30</a></li>
                            </ul><!-- /.meta -->
                            <p>Lorem ipsum dolor amet consectetur adipisicing elit sed eiusm tempor incididunt ut labore dolor magna aliqua.</p>
                        </div><!-- /.bottom-box -->
                    </div><!-- /.single-blog-post -->
                </div><!-- /.col-md-4 -->
                <div class="col-md-4 col-sm-6">
                    <div class="single-blog-list">
                        <div class="img-box">
                            <img src="{{ asset('assets/web/img/blog-s1.png')}}" alt="Awesome Image"/>
                            <div class="overlay">
                                <div class="dt">
                                    <div class="dtc">
                                        <div class="date-box">
                                            <span>20</span>May
                                        </div><!-- /.date-box -->
                                    </div><!-- /.dtc -->
                                </div><!-- /.dt -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                        <div class="text-box">
                            <h3><a href="#">Etiam Vel Neqe</a></h3>
                            <p>Lorem ipsum dolor sit amet cons ectetur elit sed do.</p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-blog-list -->
                    <div class="single-blog-list">
                        <div class="img-box">
                            <img src="{{ asset('assets/web/img/blog-s2.png') }}" alt="Awesome Image"/>
                            <div class="overlay">
                                <div class="dt">
                                    <div class="dtc">
                                        <div class="date-box">
                                            <span>20</span>May
                                        </div><!-- /.date-box -->
                                    </div><!-- /.dtc -->
                                </div><!-- /.dt -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                        <div class="text-box">
                            <h3><a href="#">Ligula Vitae Eges</a></h3>
                            <p>Lorem ipsum dolor sit amet cons ectetur elit sed do.</p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-blog-list -->
                    <div class="single-blog-list">
                        <div class="img-box">
                            <img src="{{ asset('assets/web/img/blog-s3.png')}}" alt="Awesome Image"/>
                            <div class="overlay">
                                <div class="dt">
                                    <div class="dtc">
                                        <div class="date-box">
                                            <span>20</span>May
                                        </div><!-- /.date-box -->
                                    </div><!-- /.dtc -->
                                </div><!-- /.dt -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                        <div class="text-box">
                            <h3><a href="#">Odio Convallis</a></h3>
                            <p>Lorem ipsum dolor sit amet cons ectetur elit sed do.</p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-blog-list -->
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.our-news -->


    <section class="subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Subscribe to our Newsletter</h3>
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <form action="#">
                        <input type="text" placeholder="Your Email" />
                        <button type="submit">Subscribe</button>
                    </form>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.subscribe-section -->

@endsection

{{--
@section('styles')
@endsection

@section('content')
@include('frontend.home.section.slider')
<div class="container">
@include('frontend.home.section.services')
</div>
@include('frontend.home.section.rooms')
@endsection


@section('scripts')
@endsection--}}
