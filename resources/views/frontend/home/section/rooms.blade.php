<div class="site-section block-13 bg-light">
      <div class="container">
         <div class="row mb-5">
            <div class="col-md-7 section-heading">
              <span class="subheading-sm">Featured Rooms</span>
              <h2 class="heading">Rooms &amp; Suites</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, iusto, omnis! Quidem, sint, impedit? Dicta eaque delectus tempora hic, corporis velit doloremque quod quam laborum, nobis iusto autem culpa quaerat!</p>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="nonloop-block-13 owl-carousel">
                  @foreach ($data['rooms'] as $room)
                      <div class="item">
                      <div class="block-34">
                      <div class="image">
                        <a href="#"><img src="{{ asset($room->image) }}" alt="Image placeholder"></a>
                      </div>
                      <div class="text">
                        <h2 class="heading">{{$room->room_types->title}}</h2>
                        <div class="price"><sup>Rs</sup><span class="number">{{$room->regular_price}}</span><sub>/per night</sub></div>
                        <ul class="specs">
                          <li><strong>Facilities:</strong> Closet with hangers, HD flat-screen TV, Telephone</li>
                          <li><strong>Description:</strong> {{$room->excerpt}}</li>
                          <li><strong>Bed Type:</strong> {{$room->no_bed}} bed</li>
                        </ul>
                      </div>
                      </div>
                      </div>
                  @endforeach                  
              </div>
    
            </div> <!-- .col-md-12 -->
          </div>
      </div>
    </div>