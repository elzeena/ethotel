@if($data['slider'])
<div class="block-31" style="position: relative;">
  <div class="owl-carousel loop-block-31 ">
    @foreach ($data['slider'] as $slider)
      <div class="block-30 item" style="background-image: url('{{ asset($slider->photo) }}');" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-10">
              <span class="subheading-sm">{{ $slider->caption }}</span>
              <h2 class="heading">{{ $slider->title }}</h2>
              <p><a href="{{ $slider->links }}" class="btn py-4 px-5 btn-primary">Learn More</a></p>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
@endif