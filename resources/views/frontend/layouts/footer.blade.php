
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="footer-widget about-widget">
          <a href="index.html"><img src="img/logo-2.png" alt="" /></a>
          <p>Lorem ipsum dolor amet consectetur adipisicing elit sed eiusm tempor incididunt ut labore dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamaboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit.in voluptate.</p>
          <ul class="social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul><!-- /.social -->
        </div><!-- /.footer-widget -->


      </div><!-- /.col-md-6 -->
      <div class="col-md-3">
        <div class="footer-widget link-widget">
          <div class="title">
            <h3>Link</h3>
          </div><!-- /.title -->
          <ul>
            <li><a href="#"><i class="fa fa-angle-right"></i> Company History</a></li>
            <li><a href="{{ route('about-us') }}"><i class="fa fa-angle-right"></i> About Us</a></li>
            <li><a href="{{ route('contact') }}"><i class="fa fa-angle-right"></i> Contact Us</a></li>
            <li><a href="{{ route('services') }}"><i class="fa fa-angle-right"></i> Services</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Privacy Policy</a></li>
          </ul>
        </div><!-- /.footer-widget -->
      </div><!-- /.col-md-3 -->
      <div class="col-md-3">
        <div class="footer-widget contact-widget">
          <div class="title">
            <h3>Contact Us</h3>
          </div><!-- /.title -->
          <p>Lorem ipsum dolor sit amet, consect etur adipisicing elit sed do eiusmod.</p>
          <ul>
            <li>
              <div class="icon-box">
                <i class="fa fa-map-marker"></i>
              </div><!-- /.icon-box -->
              <div class="text-box">
                <p>1201 park street,  Avenue, Dhanmondy, Dhaka.</p>
              </div><!-- /.text-box -->
            </li>
            <li>
              <div class="icon-box">
                <i class="fa fa-phone"></i>
              </div><!-- /.icon-box -->
              <div class="text-box">
                <p>[88] 657 524 332</p>
              </div><!-- /.text-box -->
            </li>
            <li>
              <div class="icon-box">
                <i class="fa fa-envelope"></i>
              </div><!-- /.icon-box -->
              <div class="text-box">
                <p>info@dentar.com</p>
              </div><!-- /.text-box -->
            </li>
          </ul>
        </div><!-- /.footer-widget -->
      </div><!-- /.col-md-3 -->
    </div><!-- /.row -->
  </div><!-- /.container -->
</footer>
<section class="bottom-footer">
  <div class="container">
    <div class="copy-text pull-left">
      <p><span>Crowny</span>  &copy;  2017 All Right Reserved</p>
    </div><!-- /.copy-text -->
    <div class="scroll-to-btn pull-right scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></dov><!-- /.scroll-to-btn pull-right -->
    </div><!-- /.container -->
</section><!-- /.bottom-footer -->

<script src="{{ asset('assets/web/assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- revolution slider js -->
<script src="{{ asset('assets/web/assets/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src={{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

<script src="{{ asset('assets/web/assets/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('assets/web/assets/zebra-datepicker/zebra_datepicker.js') }}"></script>
<script src="{{ asset('assets/web/assets/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/owl.carousel-2/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/web/assets/isotope.pkgd.min.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/web/assets/gmaps.js') }}"></script>
<!-- google map helper -->

<script src="{{ asset('assets/web/assets/map-helper.js') }}"></script>
<script src="{{ asset('assets/web/js/custom.js') }}"></script>

</body>

<!-- Mirrored from html.tonatheme.com/2017/crowny/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Aug 2018 07:11:06 GMT -->
</html>
