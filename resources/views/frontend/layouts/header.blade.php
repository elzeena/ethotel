<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.tonatheme.com/2017/crowny/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Aug 2018 07:03:41 GMT -->
<head>
    <meta charset="UTF-8" />
    <title>Crowny Hotel || Responsive HTML 5 template</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Lora:400,400i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/revolution/css/settings.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/revolution/css/layers.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/revolution/css/navigation.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/bootstrap-select/dist/css/bootstrap-select.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/zebra-datepicker/default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/owl.carousel-2/assets/owl.carousel.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/owl.carousel-2/assets/owl.theme.default.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/icomoon/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/assets/Magnific-Popup-master/dist/magnific-popup.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/web/css/responsive.css') }}" />
</head>
<body>



<header class="header header-2 ">
    <nav class="navbar navbar-default header-navigation stricky">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="{{ asset('assets/web/img/logo.png') }}" alt="Awesome Image"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav-bar">


                <ul class="nav navbar-nav navbar-right right-box">

                    <li><a class="thm-btn book-now-btn" href="#">book now</a></li>
                </ul>
                <ul class="nav navbar-nav navigation-box navbar-right">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('about-us') }}">About</a></li>
                    <li>
                        <a href=" {{route('rooms')}}">Rooms</a>

                    </li>

                    <li>
                        <a href=" ">News</a>

                    </li>
                    <li><a href="{{route('contact')}}">Contact</a></li>

                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

</header><!-- /.header -->
