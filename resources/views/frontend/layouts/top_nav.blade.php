<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand" href="{{ route('home') }}">Ktm Hotel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="oi oi-menu"></span> Menu
    </button>
    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item" {!! request()->is('home')?'class="active"':'' !!}><a href="{{route('home')}}" class="nav-link">Home</a></li>
        <li class="nav-item" {!! request()->is('rooms')?'class="active"':'' !!}><a href="{{route('rooms')}}" class="nav-link">Rooms</a></li>
        <li class="nav-item" {!! request()->is('services')?'class="active"':'' !!}><a href="{{route('services')}}" class="nav-link">Services</a></li>
        <li class="nav-item" {!! request()->is('contact')?'class="active"':'' !!}><a href="{{route('contact')}}" class="nav-link">Contact</a></li>
      </ul>
    </div>
  </div>
</nav>
<!-- END nav -->