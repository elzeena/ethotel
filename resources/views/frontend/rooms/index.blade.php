@extends('frontend.layouts.master')


@section('content')



  <section class="page-title">
    <div class="container">
      <h3 class="text-center">Our Rooms</h3>
    </div><!-- /.container -->
  </section><!-- /.page-title -->
  <section class="breadcumb">
    <div class="container">
      <ul class="list-inline text-center">
        <li><a href="#">Home</a></li>
        <li><span>Our Rooms</span></li>
      </ul><!-- /.list-inline -->
    </div><!-- /.container -->
  </section><!-- /.breadcumb -->

  <section class="our-rooms rooms-grid sec-pad">
    <div class="container">
      <div class="row">
        @foreach ($data['rooms'] as $room)
        <div class="col-md-4">
          <div class="single-room">
            <div class="img-box">
              <img src="{{ asset($room->image) }}" alt="Image placeholder">
            </div><!-- /.img-box -->
            <div class="bottom-box clearfix">
              <h3>{{ $room->room_types->title }}</h3>
              <div class="stars">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
              </div><!-- /.stars -->
              <p>{{$room->excerpt}}</p>
              <a href="{!! route('rooms.title',$room->id,$room->room_types->title) !!}" class="thm-btn">book now</a>
              <div class="price-box">
                <span class="price">${{ $room->sell_price }}</span><br />
                <span>per night</span>
              </div><!-- /.price-box -->
            </div><!-- /.bottom-box -->
          </div><!-- /.single-room -->
        </div><!-- /.col-md-4 -->
        @endforeach
      </div><!-- /.room-carousel owl-carousel owl-theme -->
    </div><!-- /.container -->
  </section><!-- /.our-room -->


  <section class="subscribe-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>Subscribe to our Newsletter</h3>
        </div><!-- /.col-md-6 -->
        <div class="col-md-6">
          <form action="#">
            <input type="text" placeholder="Your Email" />
            <button type="submit">Subscribe</button>
          </form>
        </div><!-- /.col-md-6 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section><!-- /.subscribe-section -->



@endsection




