@extends('frontend.layouts.master')


@section('content')



  <section class="page-title">
    <div class="container">
      <h3 class="text-center">Booking Form</h3>
    </div><!-- /.container -->
  </section><!-- /.page-title -->
  <section class="breadcumb">
    <div class="container">
      <ul class="list-inline text-center">
        <li><a href="#">Home</a></li>
        <li><span>Booking</span></li>
      </ul><!-- /.list-inline -->
    </div><!-- /.container -->
  </section><!-- /.breadcumb -->
  <section class="room-details-area our-services sec-pad">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card-body " id="bar-parent">
            {!! Form::open([

              'url' => route('reservation.publish')

              ])
            !!}
            <div class="form-group">
              <label for="name">Room Type </label>

              <input type="text" placeholder="Room Type" name="reservations"  value="{{ $room->room_types->title }}" class="form-control" />
            </div>
            <div class="form-group">
              <label for="name">Name </label>
              {!! Form::text('name', isset($reservations->name)?$reservations->name:'', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              {!! Form::text('email', isset($reservations->email)?$reservations->email:'', ['class' => 'form-control','placeholder'=>'Enter Email']) !!}
            </div>
            <div class="form-group">
              <label for="address">Address</label>
              {!! Form::text('address', isset($reservations->address)?$reservations->address:'', ['class' => 'form-control','placeholder'=>'Enter Address']) !!}
            </div>
            <div class="form-group">
              <label for="number">Phone No.</label>
              {!! Form::text('phone', isset($reservations->phone)?$reservations->phone:'', ['class' => 'form-control','placeholder'=>'Enter Phone No.']) !!}
            </div>
            {{-- <div class="form-group">
                <label for="arrival_date">Arrival Date</label>
                {!! Form::text('arrival_date', isset($reservations->arrival_date)?$reservations->arrival_date:'', ['class' => 'form-control','placeholder'=>'Enter Arrival Date'], ['id' => 'date']) !!}
            </div> --}}
            <div class="form-group">
              <label>Arrival Date</label>
              <input class="form-control datepicker" type="text" value="{{ $room->arrival_date }}"  placeholder="Araival date" name="arrival_date">

            </div>
            <div class="form-group">
              <label>Departure Date</label>
              <input class="form-control datepicker" type="text" value="{{ $room->departure_date }} "   placeholder="Departure date"  name="departure_date">

            </div>


            {{-- <div class="form-group">
                <label for="departure_date">Departure Date</label>
                {!! Form::text('departure_date', isset($reservations->departure_date)?$reservations->departure_date:'', ['class' => 'form-control','placeholder'=>'Enter Departure Date'], ['id' => 'date']) !!}
            </div> --}}
            <div class="form-group">
              <label for="number" type="number">Number Adults</label>
              {!! Form::text('num_adults', isset($reservations->num_adults)?$reservations->num_adults:'', ['class' => 'form-control','placeholder'=>'Enter Adults']) !!}
            </div>
            <div class="form-group">
              <label for="number" type="number">Number Kids</label>
              {!! Form::text('num_kids', isset($reservations->num_kids)?$reservations->num_kids:'', ['class' => 'form-control','placeholder'=>'Enter Kids']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('status', 'Status *', ['class' => 'col-sm-3 control-label no-padding-right']) !!}
              {!! Form::select('gender', [1 =>'Male', 0 => 'Female'], null, ['class' => 'form-control']) !!}
            </div>
            {{--  <div class="form-group">
                {!! Form::label('image', 'Image', ['class' => 'form-group']) !!}
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div> --}}
            <div class="form-group">
              <label for="total_amt">Total Amount</label>
              {!! Form::text('total_amt', isset($reservations->total_amt)?$reservations->total_amt:'', ['class' => 'form-control','placeholder' => 'Enter Amount '])!!}
            </div>
            <div class="form-group">
              {!! Form::label('satus', 'Status', ['class'=>'form-label']) !!}
              {!! Form::select('status', [0=>'Inactive',1=>'Active'],0, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{ Form::close() }}
          </div>


        </div>

      </div><!-- /.row -->
    </div><!-- /.container -->
  </section>


  <section class="subscribe-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>Subscribe to our Newsletter</h3>
        </div><!-- /.col-md-6 -->
        <div class="col-md-6">
          <form action="#">
            <input type="text" placeholder="Your Email" />
            <button type="submit">Subscribe</button>
          </form>
        </div><!-- /.col-md-6 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section><!-- /.subscribe-section -->



@endsection




