@extends('frontend.layouts.master')


@section('content')



  <section class="page-title">
    <div class="container">
      <h3 class="text-center">Our Rooms</h3>
    </div><!-- /.container -->
  </section><!-- /.page-title -->
  <section class="breadcumb">
    <div class="container">
      <ul class="list-inline text-center">
        <li><a href="#">Home</a></li>
        <li><span>Our Rooms</span></li>
      </ul><!-- /.list-inline -->
    </div><!-- /.container -->
  </section><!-- /.breadcumb -->
  <section class="room-details-area our-services sec-pad">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="room-details">
            <div class="img-box">
              <img src="{{ asset($room->image) }}" alt="Image placeholder">
            </div><!-- /.img-box -->
            <div class="details-box">
              <div class="top-box clearfix">
                <div class="title">
                  <h3>{{ $room->room_types->title }}</h3>
                  <div class="stars">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                  </div><!-- /.stars -->
                </div><!-- /.title -->
                <div class="price-box">
                  <span class="price">${{ $room->regular_price }}</span>
                  <span>per night</span>
                </div><!-- /.price-box -->
              </div><!-- /.top-box -->
              <div class="text-box">
                {{$room->description}}
              </div><!-- /.text-box -->
            </div><!-- /.details-box -->
            <div class="row">
              <div class="col-md-3">
                <div class="single-service">
                  <i class="icon-car"></i>
                  <h3>Free Parking</h3>
                </div><!-- /.single-service -->
              </div><!-- /.col-md-3 -->
              <div class="col-md-3">
                <div class="single-service">
                  <i class="icon-monitor"></i>
                  <h3>Led TV</h3>
                </div><!-- /.single-service -->
              </div><!-- /.col-md-3 -->
              <div class="col-md-3">
                <div class="single-service">
                  <i class="icon-wifi"></i>
                  <h3>Wi-Fi Service</h3>
                </div><!-- /.single-service -->
              </div><!-- /.col-md-3 -->
              <div class="col-md-3">
                <div class="single-service">
                  <i class="icon-coffee-cup"></i>
                  <h3>Breakfast</h3>
                </div><!-- /.single-service -->
              </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
          </div><!-- /.room-details -->
          <div class="title">
            <h3>Client Reviews</h3>
          </div><!-- /.title -->
          <form action="#" class="review-form">
            <p>Your Rating &emsp; <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></p>
            <textarea placeholder="Your Review"></textarea>
            <div class="row">
              <div class="col-md-6">
                <input type="text" placeholder="Your Name" />
              </div><!-- /.col-md-6 -->
              <div class="col-md-6">
                <input type="text" placeholder="Your Email" />
              </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
            <button type="submit" class="thm-btn">Submit Now</button>
          </form>
        </div><!-- /.col-md-8 -->
        <div class="col-md-4">
          <div class="reservision-box">
            <h3>Room <span>Reservation</span></h3>

            {!! Form::open([
            'url' => route('reservation.booking',$room->id) ,
            'files'=>true,
            ])
          !!}

              <div class="form-grp">
                <input class="datepicker" type="text"  placeholder="Araival date" name="arrival_date">


                <i class="fa fa-calendar"></i>
              </div><!-- /.form-grp -->
              <div class="form-grp">
                <input class="datepicker" type="text"   placeholder="Departure date"  name="departure_date">

                <i class="fa fa-calendar"></i>
              </div><!-- /.form-grp -->
              <div class="form-grp">
                {!! Form::text('num_adults', isset($reservations->num_adults)?$reservations->num_adults:'', ['class' => 'form-control','placeholder'=>'Enter Adults']) !!}
              </div><!-- /.form-grp -->
              <div class="form-grp">
                {!! Form::text('num_kids', isset($reservations->num_kids)?$reservations->num_kids:'', ['class' => 'form-control','placeholder'=>'Enter Kids']) !!}
              </div><!-- /.form-grp -->
              <div class="form-grp">
                <input type="text" placeholder="Roome Type" name="room_type"  value="{{ $room->room_types->title }}" class="" />
              </div><!-- /.form-grp -->
              <div class="form-grp">

                <button class="thm-btn" type="submit">Book Now</button>
              </div><!-- /.form-grp -->
            {{ Form::close() }}
          </div><!-- /.reservision-box -->
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section>


  <section class="subscribe-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>Subscribe to our Newsletter</h3>
        </div><!-- /.col-md-6 -->
        <div class="col-md-6">
          <form action="#">
            <input type="text" placeholder="Your Email" />
            <button type="submit">Subscribe</button>
          </form>
        </div><!-- /.col-md-6 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section><!-- /.subscribe-section -->



@endsection




