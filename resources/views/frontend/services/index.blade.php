@extends('frontend.layouts.master')
@section('styles')
@endsection

@section('content')
<div class="block-30 block-30-sm item" style="background-image: url('{{ asset('assets/frontend/images/bg_2.jpg') }}');" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-10">
        <span class="subheading-sm"></span>
        <h2 class="heading"></h2>
      </div>
    </div>
  </div>
</div>
<div class="site-section bg-light">
<div class="container">
  <div class="row site-section">
  <div class="col-md-12">
    <div class="row mb-5">
      <div class="col-md-7 section-heading">
        <span class="subheading-sm">Services</span>
        <h2 class="heading">Facilities &amp; Services</h2>
      </div>
    </div>
  </div>
  @foreach ($data['services'] as $services)
   <div class="col-md-6 col-lg-4">
        <div class="media block-6">
          <div class="icon"><img src="{{ asset($services->service_img) }}" style="width: 100px;height: auto;"></div>
          <div class="media-body">
            <h3 class="heading">{{$services->title}}</h3>
            <p>{{$services->short_description}}</p>
          </div>
        </div>      
    </div>
  @endforeach
</div>

</div>
</div>

@endsection


@section('scripts')
@endsection


