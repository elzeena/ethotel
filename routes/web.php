<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'auth'], function(){
 			
 			Route::get('dashboard',  ['as' => 'dashboard',  'uses' => 'DashboardController@index']);
 			Route::get('profile',  ['as' => 'profile',  'uses' => 'UserController@profile']);
 			Route::post('profile',  ['as' => 'profile.update',  'uses' => 'UserController@profileUpdate']);
 			//Route::resource('user', 'UserController');
 			Route::resource('roomtypes', 'RoomTypeController');
 			Route::resource('hotel', 'HotelController');
 			Route::resource('reservation', 'ReservationController');
 			Route::resource('inquiry', 'InquiryController');
 			Route::get('reservation/publish/{id}', 'ReservationController@publish')->name('reservation.publish');
 			Route::get('reservation/genderone/{id}', 'ReservationController@genderone')->name('reservation.genderone');

 			Route::resource('addstaff', 'AddStaffController');
 			Route::resource('room', 'RoomController');


			Route::resource('service', 'ServicesController');
 			Route::resource('slider', 'SliderController');
 			Route::get('slider/publish/{id}', 'SliderController@publish')->name('slider.publish');
 			
 			Route::resource('amenity', 'AmenityController');

 			Route::get('roomtypes/{id}/delete', ['as' => 'roomtypes.delete', 'uses' => 'RoomTypeController@destroy']);


 			Route::get('room/{id}/delete', ['as' => 'room.delete', 'uses' => 'RoomController@destroy']);




});

Auth::routes();

/*FrontEnd*/
Route::get('/', 'Web\HomeController@index')->name('home');
Route::get('/about-us', 'Web\HomeController@aboutUs')->name('about-us');
Route::get('/services', 'Web\HomeController@services')->name('services');
Route::get('/rooms', 'Web\RoomsController@index')->name('rooms');
Route::get('reservation/{id}', 'Web\RoomsController@booking')->name('reservation.booking');
Route::get('reservation/publish/', 'Web\RoomsController@publish')->name('reservation.publish');
Route::get('/rooms/{id}', 'Web\RoomsController@getSingleRoom')->name('rooms.title');
Route::get('/contact', 'Web\HomeController@contact')->name('contact');
Route::post('/submitContactForm', 'Web\HomeController@submitContactForm')->name('submitContactForm');






